<div class="col-md-3 left_col menu_fixed">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ route('admin.home') }}" class="site_title"><i class="fa fa-paw"></i> <span>{{ config('app.name') }}</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="{{ asset('images/img.jpg') }}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Добро пожаловать,</span>
                <h2>{{ Auth::user()->name }}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Главное</h3>
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-plane"></i> Авиа <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('admin.update_errors') }}">Update errors</a></li>
                            <li><a href="{{ route('admin.update_warnings') }}">Update warnings</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-user-secret"></i> Администраторы <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('users.index') }}">Список администраторов</a></li>
                            <li><a href="{{ route('users.create') }}">Добавить администратора</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->

        <!-- menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <form action="{{ route('logout') }}" method="post" name="FormLogout1">
                @csrf
                <a data-toggle="tooltip" data-placement="top" title="{{ __('auth.logout') }}" href='#' onclick = "document.FormLogout1.submit ();">
                    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                </a>
            </form>
        </div>
        <!-- /menu footer buttons -->

    </div>
</div>
