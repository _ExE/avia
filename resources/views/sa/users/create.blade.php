@extends('sa.layouts.sa')

@section('main')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Добавить администратора</h3>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                <form method="post" action="{{ portRoute('users.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="name">ФИО:</label>
                        <input type="text" class="form-control" name="name"/>
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="text" class="form-control" name="email"/>
                    </div>
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </form>
            </div>
        </div>
    </div>
@endsection
