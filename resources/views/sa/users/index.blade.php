@extends('sa.layouts.sa')

@section('main')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Список администраторов</h3>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">

            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>
        <div class="col-sm-12">
            <div>
                <a style="margin: 19px;" href="{{ portRoute('users.create')}}" class="btn btn-primary">Добавить</a>
            </div>
            <table class="table table-striped">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>ФИО</td>
                    <td>Email</td>
                    <td colspan = 2>Действия</td>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            <a href="{{ portRoute('users.edit',$user->id)}}" class="btn btn-primary">Редактировать</a>
                        </td>
                        <td>
                            <form action="{{ portRoute('users.destroy', $user->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Удалить</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div>
            </div>
@endsection
