@extends('sa.layouts.sa')

@section('link')
    @parent
    <!-- Datatables -->
    <link href="{{ asset('admin/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    {{--    <link href="{{ asset('admin/css/buttons.bootstrap.min.css') }}" rel="stylesheet">--}}
    {{--    <link href="{{ asset('admin/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">--}}
    {{--    <link href="{{ asset('admin/css/responsive.bootstrap.min.css') }}" rel="stylesheet">--}}
    {{--    <link href="{{ asset('admin/css/scroller.bootstrap.min.css') }}" rel="stylesheet">--}}
@endsection

@section('main')

    <div class="">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <div class="page-title">
            <div class="title_left">
                <h3>Главная <small></small></h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-10 col-sm-10 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Даты обновления <small>avia</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Тип данных</th>
                                <th>MIN</th>
                                <th>MAX</th>
                                <th>Loaded</th>
                                <th>Inserted</th>
                                <th>Errors</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($updateDates as $item)
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{$item->data_type}}</td>
                                    <td>{{$item->minDate}}</td>
                                    <td>{{$item->maxDate}}</td>
                                    <td>{{$item->loaded}}</td>
                                    <td>{{$item->inserted}}</td>
                                    <td>{{$item->errorCount}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('js')
    @parent
    <!-- Datatables -->
    <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/dataTables.bootstrap.min.js') }}"></script>
    {{--    <script src="{{ asset('admin/js/dataTables.buttons.js') }}"></script>--}}
@endsection
