<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

@section('link')
    <!-- Bootstrap -->
        <link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ asset('admin/css/font-awesome.min.css') }}" rel="stylesheet">
        <!-- NProgress -->
{{--        <link href="{{ asset('admin/css/nprogress.css') }}" rel="stylesheet">--}}
        <!-- jQuery custom content scroller -->
        <link href="{{ asset('admin/css/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet"/>
@show
<!-- Custom Theme Style -->
    <link href="{{ asset('admin/css/custom.min.css') }}" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

    @include('sa.inc.left_menu')
    @include('sa.inc.top_nav')

    <!-- page content -->
        <div class="right_col" role="main">
            @yield('main')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="{{ asset('admin/js/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('admin/js/bootstrap.min.js') }}"></script>
@section('js')
    <!-- FastClick -->
{{--    <script src="{{ asset('admin/js/fastclick.min.js') }}"></script>--}}
    <!-- NProgress -->
{{--    <script src="{{ asset('admin/js/nprogress.min.js') }}"></script>--}}
    <!-- jQuery custom content scroller -->
{{--    <script src="{{ asset('admin/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>--}}
@show
<!-- Custom Theme Scripts -->
<script src="{{ asset('admin/js/custom.js') }}"></script>

</body>
</html>
