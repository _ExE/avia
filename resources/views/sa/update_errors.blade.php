@extends('sa.layouts.sa')

@section('link')
    @parent
    <!-- Datatables -->
    <link href="{{ asset('admin/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
@endsection

@section('main')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Последние ошибки <small>по типам данных</small></h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            @foreach($updateResults as $dataType=>$results)
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">

                        <div class="x_title">
                            <h2><small>Тип данных: </small>{{$dataType}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">
                            <table id="datatable_{{$loop->iteration}}" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Дата:время</th>
                                    <th>Раз</th>
                                    <th>Сообщение</th>
                                    <th>URL</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($results as $item)
                                    <tr title="{{$item->url}}">
                                        <td>{{$item->updated_at}}</td>
                                        <td>{{$item->error_count}}</td>
                                        <td>{{Str::substr($item->error['message'],0,500)}}</td>
                                        <td>{{Str::substr(parse_url($item->url, PHP_URL_QUERY),0,50)}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('js')
    @parent
    <!-- Datatables -->
    <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/dataTables.bootstrap.min.js') }}"></script>
@endsection
