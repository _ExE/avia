@extends('auth.layouts.auth')

@section('content')
    <form method="POST" action="{{ portRoute('login') }}">
        @csrf
        <h1>Авторизация</h1>
        <div>
            <input id="email" type="email" placeholder="{{ __('email') }}"
                   class="form-control @error('email') is-invalid @enderror"
                   name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
            @enderror
        </div>
        <div>
            <input id="password" type="password" placeholder="{{ __('custom.Password') }}"
                   class="form-control @error('password') is-invalid @enderror" name="password"
                   required autocomplete="current-password">
            @error('password')
            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
            @enderror
        </div>
        <div>
            <button type="submit" class="btn btn-primary">
                {{ __('custom.Login') }}
            </button>
            <input class="form-check-input" type="checkbox" name="remember"
                   id="remember" {{ old('remember') ? 'checked' : '' }}>
            <label class="form-check-label" for="remember">
                {{ __('custom.Remember Me') }}
            </label>
            <div class="clearfix"></div>

            <div class="separator">
                @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ portRoute('password.request') }}">
                        {{ __('custom.Forgot Your Password?') }}
                    </a>
                @endif
                <div>
                    <h1><i class="fa fa-paw"></i> {{ config('app.name') }}</h1>
                    <p>©2020 Все права защищены.</p>
                </div>
            </div>
        </div>
    </form>
@endsection
