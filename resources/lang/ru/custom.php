<?php

return [
    'Login' => 'Войти',
    'Password' => 'Пароль',
    'Remember Me' => 'Запомнить меня',
    'Forgot Your Password?' => 'Забыли пароль?',
    'Reset Password' => 'Изменить пароль',
    'Send Password Reset Link' => 'Получить ссылку для смены пароля',
];
