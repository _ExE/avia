<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UpdateResultController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Avia\AviaHomeController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;


Route::domain('avia.' . config('app.domain'))->group(static function () {
    Route::get('/', [AviaHomeController::class, 'index'])
        ->name('avia.home');
});

Route::prefix('sa')->middleware('auth')->group(static function () {
    Route::get('/', [DashboardController::class, 'index'])
        ->name('admin.home');
    Route::get('/update_errors', [UpdateResultController::class, 'latestErrors'])
        ->name('admin.update_errors');
    Route::get('/update_warnings', [UpdateResultController::class, 'latestWarnings'])
        ->name('admin.update_warnings');
    Route::resource('users', UserController::class);
});

Auth::routes(['register' => false]);

Route::get('/', [HomeController::class, 'index'])
    ->name('home');
