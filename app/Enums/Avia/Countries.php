<?php


namespace App\Enums\Avia;

use App\Enums\Enumeration;

/**
 * @method static static RU();
 */
class Countries extends Enumeration
{
    public const RU = 'RU';

}
