<?php


namespace App\Enums\Avia;

use App\Enums\Enumeration;

/**
 * @method static static AIRPORT();
 * @method static static BUS();
 * @method static static HARBOUR();
 * @method static static HELIPORT();
 * @method static static MILITARY();
 * @method static static RAILWAY();
 * @method static static SEAPLANE();
*/
class IataType extends Enumeration
{
    public const AIRPORT = 'airport';
    public const BUS = 'bus';
    public const HARBOUR = 'harbour';
    public const HELIPORT = 'heliport';
    public const MILITARY = 'military';
    public const RAILWAY = 'railway';
    public const SEAPLANE = 'seaplane';
}
