<?php


namespace App\Enums\Avia;

use App\Enums\Enumeration;

/**
 * @method static static RU();
 * @method static static EN();
*/
class Language extends Enumeration
{
    public const RU = 'ru';
    public const EN = 'en';

}
