<?php

namespace App\Enums\Avia;


use App\Enums\Enumeration;

/**
 * @method static static AIRLINES();        * Данные об авиакомпаниях в json формате
 * @method static static AIRPORTS();        * Данные об аэропортах в json формате
 * @method static static ALLIANCES();       * Данные об альянсах в json формате
 * @method static static CITIES();          * Данные о городах в json формате
 * @method static static CITY_DIRECTIONS(); * Популярные направления из города
 * @method static static COUNTRIES();       * Данные о странах в json формате
 * @method static static PLANES();          * Данные о самолетах в json формате
 * @method static static PRICES();          * Цены на авиабилеты
 * @method static static ROUTES();          * Данные о маршрутах в json формате
*/
class DataType extends Enumeration
{
    public const AIRLINES = 'airlines';
    public const AIRPORTS = 'airports';
    public const ALLIANCES = 'alliances';
    public const CITIES = 'cities';
    public const CITY_DIRECTIONS = 'city_directions';
    public const COUNTRIES = 'countries';
    public const PLANES = 'planes';
    public const PRICES = 'prices';
    public const ROUTES = 'routes';
}
