<?php

namespace App\Enums;


use Eloquent\Enumeration\AbstractEnumeration;
use JsonSerializable;

abstract class Enumeration extends AbstractEnumeration implements JsonSerializable
{
    /**
     * @return array
     */
    public static function values(): array
    {
        return array_map(static function (self $item) {
            return $item->value();
        }, static::members());
    }

    public function jsonSerialize()
    {
        return self::values();
    }

}
