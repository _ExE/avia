<?php

namespace App\Exceptions\Avia;

use Exception;

class UpdateException extends Exception
{
    public function report(): void
    {
        logError($this);
    }

}
