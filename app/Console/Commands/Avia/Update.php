<?php

namespace App\Console\Commands\Avia;

use App\Enums\Avia\DataType;
use App\Exceptions\Avia\UpdateException;
use App\Models\Avia\AviaUpdateResult;
use App\Services\Avia\Update\Handlers\AirlinesHandler;
use App\Services\Avia\Update\Handlers\AirportsHandler;
use App\Services\Avia\Update\Handlers\AlliancesHandler;
use App\Services\Avia\Update\Handlers\CitiesHandler;
use App\Services\Avia\Update\Handlers\CityDirectionsHandler;
use App\Services\Avia\Update\Handlers\CountriesHandler;
use App\Services\Avia\Update\Handlers\PlanesHandler;
use App\Services\Avia\Update\Handlers\PricesHandler;
use App\Services\Avia\Update\Handlers\RoutesHandler;
use Illuminate\Console\Command;
use Throwable;

class Update extends Command
{
    /**
     * @var string
     */
    protected $signature = 'avia:update
    {type : Значение константы DataType}
    {--origin= : Аэропорт вылета (не обязательно)}
    {--destination= : Аэропорт прилета (не обязательно)}
    {--page= : Страница}';

    /**
     * @var string
     */
    protected $description = 'Загрузить данные travelpayouts.com';


    public function handle(): void
    {
        $dataType = DataType::memberByValue($this->argument('type'));
        $this->alert($dataType->value());

        try {
            $result = $this->doUpdate($dataType);
            $this->printResult($result);
        } catch (Throwable $e) {
            $this->error('Error: ' . $e->getCode());
            $this->warn($e->getMessage());
            $this->info($e->getFile() . ' : ' . $e->getLine());
            logError($e);
        }
    }

    /**
     * @param DataType $type
     * @return AviaUpdateResult
     * @throws UpdateException
     */
    private function doUpdate(DataType $type): AviaUpdateResult
    {
        switch ($type) {
            case DataType::AIRLINES():
                return app(AirlinesHandler::class)->update();
            case DataType::AIRPORTS():
                return app(AirportsHandler::class)->update();
            case DataType::ALLIANCES():
                return app(AlliancesHandler::class)->update();
            case DataType::CITIES():
                return app(CitiesHandler::class)->update();
            case DataType::CITY_DIRECTIONS():
                return app(CityDirectionsHandler::class)
                    ->setOrigin($this->option('origin'))
                    ->update();
            case DataType::COUNTRIES():
                return app(CountriesHandler::class)->update();
            case DataType::PLANES():
                return app(PlanesHandler::class)->update();
            case DataType::PRICES():
                $updater = app(PricesHandler::class);
                if (null !== $this->option('origin')) {
                    $updater->setOrigin($this->option('origin'));
                }
                if (null !== $this->option('destination')) {
                    $updater->setDestination($this->option('destination'));
                }
                if (null !== $this->option('page')) {
                    $updater->setPage($this->option('page'));
                }
                return $updater->update();
            case DataType::ROUTES():
                return app(RoutesHandler::class)->update();
        }
        throw new UpdateException('DataType ' . $type->value() . ' не обрабатывается');
    }

    /**
     * @param AviaUpdateResult $result
     */
    private function printResult(AviaUpdateResult $result): void
    {
        if ($result->error !== null) {
            $this->warn(print_r($result->error,true));
        } else {
            $this->info('Loaded: ' . $result->loaded);
            $this->info('Inserted: ' . $result->inserted);
            if($result->warnings !== null){
                $this->info('Warnings: ' . count($result->warnings));
            }
        }
    }

}
