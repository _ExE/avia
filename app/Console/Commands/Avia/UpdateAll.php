<?php

namespace App\Console\Commands\Avia;

use App\Enums\Avia\Countries;
use App\Enums\Avia\DataType;
use App\Jobs\Avia\CityDirectionsUpdateJob;
use App\Jobs\Avia\PricesUpdateJob;
use App\Jobs\JobQueues;
use App\Models\Avia\AviaCity;
use App\Models\Avia\AviaCityDirection;
use App\Models\Avia\AviaPrice;
use App\Models\Avia\AviaUpdateResult;
use App\Services\Avia\Update\Handlers\CityDirectionsHandler;
use App\Services\Avia\Update\Handlers\PricesHandler;
use Illuminate\Console\Command;
use Throwable;

class UpdateAll extends Command
{
    /**
     * @var string
     */
    protected $signature = 'avia:updateAll
    {--update=on : запрашивать данные с travelpayouts.com}
    {--createJobs=on : создавать Jobs}
    ';

    /**
     * @var string
     */
    protected $description = 'Полное обновление данных travelpayouts.com';


    public function handle(): void
    {
        if ($this->option('update') === 'on') {
            $this->doUpdate();
        }

        if ($this->option('createJobs') === 'on') {
            $this->clearTables();
            $this->createJobs();
        }
    }

    private function doUpdate(): void
    {
        $this->call(Update::class, [
            'type' => DataType::AIRPORTS()->value(),
        ]);

        $this->call(Update::class, [
            'type' => DataType::AIRLINES()->value(),
        ]);

        $this->call(Update::class, [
            'type' => DataType::ALLIANCES()->value(),
        ]);

        $this->call(Update::class, [
            'type' => DataType::COUNTRIES()->value(),
        ]);

        $this->call(Update::class, [
            'type' => DataType::CITIES()->value(),
        ]);

        $this->call(Update::class, [
            'type' => DataType::PLANES()->value(),
        ]);

        $this->call(Update::class, [
            'type' => DataType::ROUTES()->value(),
        ]);
    }

    private function createJobs(): void
    {
        AviaCity::where('country_code', Countries::RU()->value())->each(function (AviaCity $aviaCity) {
            if ($this->needUpdateCityDirections($aviaCity->code)) {
                CityDirectionsUpdateJob::dispatch($aviaCity->code)->onQueue(JobQueues::AVIA_NORMAL);
            }
            if ($this->needUpdatePrices($aviaCity->code)) {
                PricesUpdateJob::dispatch($aviaCity->code)->onQueue(JobQueues::AVIA_NORMAL);
            }
        });
    }

    /**
     * @param string $code
     * @return bool
     */
    private function needUpdateCityDirections(string $code): bool
    {
        $url = (new CityDirectionsHandler())->setOrigin($code)->getUrl();
        return $this->needUpdate($url);
    }

    /**
     * @param string $code
     * @return bool
     */
    private function needUpdatePrices(string $code): bool
    {
        $url = (new PricesHandler())->setOrigin($code)->getUrl();
        return $this->needUpdate($url);
    }

    /**
     * @param string $url
     * @return bool
     */
    private function needUpdate(string $url): bool
    {
        $aviaUpdateResult = AviaUpdateResult::find(md5($url));
        if ($aviaUpdateResult !== null) {
            if ($aviaUpdateResult->error_count === 0) {
                return true;
            }
            $difDays = now()->diffInDays($aviaUpdateResult->updated_at);
            /* чем больше ошибок, тем больше дней задержки */
            return $difDays > $aviaUpdateResult->error_count;
        }
        return true;
    }

    private function clearTables(): void
    {
        try {
            AviaCityDirection::where('expires_at', '<', now())->delete();
            AviaPrice::where('depart_date', '<', now()->startOfDay())->delete();
        } catch (Throwable $e) {
            $this->error('Error: ' . $e->getCode());
            $this->warn($e->getMessage());
            $this->info($e->getFile() . ' : ' . $e->getLine());
            logError($e);
        }
    }

}

