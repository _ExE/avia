<?php

namespace App\Http\Controllers\Avia;

use App\Http\Controllers\Controller;
use App\Models\Avia\AviaCountry;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class AviaHomeController extends Controller
{
    /**
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        $countries = AviaCountry::orderBy('name_ru')->get();
        return view('avia.home', compact('countries'));
    }

}
