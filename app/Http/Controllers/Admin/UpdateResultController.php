<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Avia\Admin\UpdateResultService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class UpdateResultController extends Controller
{
    private UpdateResultService $updateResultService;

    /**
     * AdminHomeController constructor.
     * @param UpdateResultService $updateResultService
     */
    public function __construct(UpdateResultService $updateResultService)
    {
        $this->updateResultService = $updateResultService;
    }

    /**
     * @return Application|Factory|Response|View
     */
    public function latestErrors()
    {
        $updateResults = $this->updateResultService->getLatestErrorsGroupByTypes();
        return view('sa.update_errors',compact('updateResults'));
    }

    /**
     * @return Application|Factory|Response|View
     */
    public function latestWarnings()
    {
        $updateResults = $this->updateResultService->getLatestWarningsGroupByTypes();
        return view('sa.update_warnings',compact('updateResults'));
    }
}
