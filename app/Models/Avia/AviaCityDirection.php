<?php

namespace App\Models\Avia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Avia\AviaCityDirection
 *
 * @property string $id
 * @property string $origin_code пункт отправления
 * @property string $destination_code пункт назначения
 * @property int $price стоимость перелета, в указанной валюте.
 * @property int $transfers количество пересадок
 * @property string $airline_code IATA код авиакомпании
 * @property string $flight_number номер рейса
 * @property \Illuminate\Support\Carbon $departure_at дата и время отправления
 * @property \Illuminate\Support\Carbon $return_at дата и время возвращения
 * @property \Illuminate\Support\Carbon $expires_at Дата истечения срока действия найденной цены (UTC+0)
 * @property \Illuminate\Support\Carbon $created_at
 * @property-read \App\Models\Avia\AviaAirline $Airline
 * @property-read \App\Models\Avia\AviaAirport $Destination
 * @property-read \App\Models\Avia\AviaAirport $Origin
 * @method static \Illuminate\Database\Eloquent\Builder|AviaCityDirection newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AviaCityDirection newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AviaCityDirection query()
 * @method static \Illuminate\Database\Eloquent\Builder|AviaCityDirection whereAirlineCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaCityDirection whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaCityDirection whereDepartureAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaCityDirection whereDestinationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaCityDirection whereExpiresAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaCityDirection whereFlightNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaCityDirection whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaCityDirection whereOriginCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaCityDirection wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaCityDirection whereReturnAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaCityDirection whereTransfers($value)
 * @mixin \Eloquent
 */
class AviaCityDirection extends Model
{
    use \App\Models\Traits\UsesUuid;

    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'departure_at',
        'return_at',
        'expires_at',
        'created_at',
    ];

    public function Origin(): BelongsTo
    {
        return $this->belongsTo(AviaAirport::class, 'code', 'origin_code');
    }

    public function Destination(): BelongsTo
    {
        return $this->belongsTo(AviaAirport::class, 'code', 'destination_code');
    }

    public function Airline(): BelongsTo
    {
        return $this->belongsTo(AviaAirline::class, 'code', 'airline_code');
    }

}
