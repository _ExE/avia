<?php

namespace App\Models\Avia;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Avia\AviaCity
 *
 * @property string $code IATA код города
 * @property string $country_code IATA код страны, в которой находится город
 * @property string $name_en название города EN
 * @property string $name_ru название города RU
 * @property string $time_zone часовой пояс относительно гринвича
 * @property string $coordinates_lon координаты города - долгота
 * @property string $coordinates_lat координаты города - широта
 * @property Carbon $created_at
 * @property-read Collection|AviaAirport[] $Airports
 * @property-read int|null $airports_count
 * @property-read AviaCountry $Country
 * @method static Builder|AviaCity newModelQuery()
 * @method static Builder|AviaCity newQuery()
 * @method static Builder|AviaCity query()
 * @method static Builder|AviaCity whereCode($value)
 * @method static Builder|AviaCity whereCoordinatesLat($value)
 * @method static Builder|AviaCity whereCoordinatesLon($value)
 * @method static Builder|AviaCity whereCountryCode($value)
 * @method static Builder|AviaCity whereCreatedAt($value)
 * @method static Builder|AviaCity whereNameEn($value)
 * @method static Builder|AviaCity whereNameRu($value)
 * @method static Builder|AviaCity whereTimeZone($value)
 * @mixin \Eloquent
 */
class AviaCity extends Model
{
    protected $table = 'avia_cities';
    protected $primaryKey = 'code';
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = ['created_at'];
    protected $casts = ['cases' => 'array'];

    public function getIncrementing(): bool
    {
        return false;
    }

    public function getKeyType(): string
    {
        return 'string';
    }

    public function Country(): BelongsTo
    {
        return $this->belongsTo(AviaCountry::class, 'code', 'country_code');
    }

    public function Airports(): HasMany
    {
        return $this->hasMany(AviaAirport::class, 'city_code', 'code');
    }

}
