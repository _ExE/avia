<?php

namespace App\Models\Avia;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Avia\AviaUpdateResult
 *
 * @property string $md уникальный Хеш GET(POST) запроса
 * @property string $url url ресурса
 * @property int $loaded получено строк
 * @property int $inserted сохранено строк
 * @property array|null $error ошибка
 * @property array|null $warnings предупреждения
 * @property int $error_count ошибочных попыток
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|AviaUpdateResult newModelQuery()
 * @method static Builder|AviaUpdateResult newQuery()
 * @method static Builder|AviaUpdateResult query()
 * @method static Builder|AviaUpdateResult whereCreatedAt($value)
 * @method static Builder|AviaUpdateResult whereError($value)
 * @method static Builder|AviaUpdateResult whereErrorCount($value)
 * @method static Builder|AviaUpdateResult whereInserted($value)
 * @method static Builder|AviaUpdateResult whereLoaded($value)
 * @method static Builder|AviaUpdateResult whereMd($value)
 * @method static Builder|AviaUpdateResult whereUpdatedAt($value)
 * @method static Builder|AviaUpdateResult whereUrl($value)
 * @method static Builder|AviaUpdateResult whereWarnings($value)
 * @mixin \Eloquent
 */
class AviaUpdateResult extends Model
{
    Use HasFactory;

    protected $primaryKey = 'md';
    protected $guarded = [];
    protected $casts = [
        'error' => 'array',
        'warnings' => 'array',
    ];

    public function getIncrementing(): bool
    {
        return false;
    }

    public function getKeyType(): string
    {
        return 'string';
    }

    public function setUrlAttribute(string $value): self
    {
        $this->attributes['md'] = md5($value);
        $this->attributes['url'] = $value;
        return $this;
    }
}
