<?php

namespace App\Models\Avia;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Avia\AviaAirport
 *
 * @property string $code IATA код
 * @property string $country_code IATA код страны
 * @property string|null $city_code IATA код города
 * @property string $iata_type тип
 * @property int $flightable является ли действующим аэропортом
 * @property string $name_en название аэропорта EN
 * @property string $name_ru название аэропорта RU
 * @property string|null $time_zone часовой пояс относительно гринвича
 * @property string|null $coordinates_lon координаты города - долгота
 * @property string|null $coordinates_lat координаты города - широта
 * @property Carbon $created_at
 * @property-read AviaCity $City
 * @property-read Collection|AviaCityDirection[] $CityDirectionDestinations
 * @property-read int|null $city_direction_destinations_count
 * @property-read Collection|AviaCityDirection[] $CityDirectionOrigins
 * @property-read int|null $city_direction_origins_count
 * @property-read AviaCountry $Country
 * @property-read Collection|AviaPrice[] $PriceDestinations
 * @property-read int|null $price_destinations_count
 * @property-read Collection|AviaPrice[] $PriceOrigins
 * @property-read int|null $price_origins_count
 * @method static Builder|AviaAirport newModelQuery()
 * @method static Builder|AviaAirport newQuery()
 * @method static Builder|AviaAirport query()
 * @method static Builder|AviaAirport whereCityCode($value)
 * @method static Builder|AviaAirport whereCode($value)
 * @method static Builder|AviaAirport whereCoordinatesLat($value)
 * @method static Builder|AviaAirport whereCoordinatesLon($value)
 * @method static Builder|AviaAirport whereCountryCode($value)
 * @method static Builder|AviaAirport whereCreatedAt($value)
 * @method static Builder|AviaAirport whereFlightable($value)
 * @method static Builder|AviaAirport whereIataType($value)
 * @method static Builder|AviaAirport whereNameEn($value)
 * @method static Builder|AviaAirport whereNameRu($value)
 * @method static Builder|AviaAirport whereTimeZone($value)
 * @mixin \Eloquent
 */
class AviaAirport extends Model
{
    protected $primaryKey = 'code';
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = ['created_at'];

    public function getIncrementing(): bool
    {
        return false;
    }

    public function getKeyType(): string
    {
        return 'string';
    }

    public function Country(): BelongsTo
    {
        return $this->belongsTo(AviaCountry::class, 'code', 'country_code');
    }

    public function City(): BelongsTo
    {
        return $this->belongsTo(AviaCity::class, 'code', 'city_code');
    }

    public function CityDirectionOrigins(): HasMany
    {
        return $this->hasMany(AviaCityDirection::class, 'origin_code', 'code');
    }

    public function CityDirectionDestinations(): HasMany
    {
        return $this->hasMany(AviaCityDirection::class, 'destination_code', 'code');
    }

    public function PriceOrigins(): HasMany
    {
        return $this->hasMany(AviaPrice::class, 'origin_code', 'code');
    }

    public function PriceDestinations(): HasMany
    {
        return $this->hasMany(AviaPrice::class, 'destination_code', 'code');
    }

}
