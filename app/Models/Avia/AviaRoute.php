<?php

namespace App\Models\Avia;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Avia\AviaRoute
 *
 * @property int $id
 * @property string $airline_iata  IATA-код авиакомпании
 * @property string $departure_airport_iata IATA-код аэропорта отправления
 * @property string $arrival_airport_iata IATA-код аэропорта прибытия
 * @property int $codeshare показывает осуществляет ли рейс та же компания, что продает билет
 * @property int $transfers количество пересадок
 * @property mixed $planes IATA-коды самолетов
 * @property \Illuminate\Support\Carbon $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|AviaRoute newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AviaRoute newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AviaRoute query()
 * @method static \Illuminate\Database\Eloquent\Builder|AviaRoute whereAirlineIata($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaRoute whereArrivalAirportIata($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaRoute whereCodeshare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaRoute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaRoute whereDepartureAirportIata($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaRoute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaRoute wherePlanes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaRoute whereTransfers($value)
 * @mixin \Eloquent
 */
class AviaRoute extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
    protected $dates = ['created_at'];

}
