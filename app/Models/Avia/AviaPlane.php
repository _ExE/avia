<?php

namespace App\Models\Avia;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Avia\AviaPlane
 *
 * @property string $code iata-код самолета
 * @property string $name название самолета
 * @property \Illuminate\Support\Carbon $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPlane newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPlane newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPlane query()
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPlane whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPlane whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPlane whereName($value)
 * @mixin \Eloquent
 */
class AviaPlane extends Model
{
    protected $primaryKey = 'code';
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = ['created_at'];

    public function getIncrementing(): bool
    {
        return false;
    }

    public function getKeyType(): string
    {
        return 'string';
    }
}
