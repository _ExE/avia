<?php

namespace App\Models\Avia;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Avia\AviaAirline
 *
 * @property string $code IATA-код авиакомпании
 * @property string $name_en название авиакомпании EN
 * @property string $name_ru название авиакомпании RU
 * @property Carbon $created_at
 * @property-read Collection|AviaCityDirection[] $CityDirections
 * @property-read int|null $city_directions_count
 * @method static Builder|AviaAirline newModelQuery()
 * @method static Builder|AviaAirline newQuery()
 * @method static Builder|AviaAirline query()
 * @method static Builder|AviaAirline whereCode($value)
 * @method static Builder|AviaAirline whereCreatedAt($value)
 * @method static Builder|AviaAirline whereNameEn($value)
 * @method static Builder|AviaAirline whereNameRu($value)
 * @mixin \Eloquent
 */
class AviaAirline extends Model
{
    protected $primaryKey = 'code';
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = ['created_at'];

    public function getIncrementing(): bool
    {
        return false;
    }

    public function getKeyType(): string
    {
        return 'string';
    }

    public function CityDirections(): HasMany
    {
        return $this->hasMany(AviaCityDirection::class, 'airline_code', 'code');
    }

}
