<?php

namespace App\Models\Avia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Avia\AviaPrice
 *
 * @property string $id
 * @property string $origin_code пункт отправления
 * @property string $destination_code пункт назначения
 * @property int $show_to_affiliates False-все цены, true-только цены, найденные с помощью маркера партнера (рекомендуется)
 * @property int $trip_class Класс полета: 0-эконом-класс, 1 — Бизнес — класс, 2-первый класс
 * @property string $depart_date дата  отправления
 * @property string $return_date дата  возвращения
 * @property int $number_of_changes количество пересадок
 * @property string $value Стоимость перелета, в указанной валюте
 * @property string $found_at Время и дата, на которые был найден билет
 * @property int $distance Расстояние между пунктом отправления и пунктом назначения
 * @property int $actual Актуальность предложения
 * @property \Illuminate\Support\Carbon $created_at
 * @property-read \App\Models\Avia\AviaAirport $Destination
 * @property-read \App\Models\Avia\AviaAirport $Origin
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice whereActual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice whereDepartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice whereDestinationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice whereDistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice whereFoundAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice whereNumberOfChanges($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice whereOriginCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice whereReturnDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice whereShowToAffiliates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice whereTripClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AviaPrice whereValue($value)
 * @mixin \Eloquent
 */
class AviaPrice extends Model
{
    use \App\Models\Traits\UsesUuid;

    protected $guarded = [];
    public $timestamps = false;
    protected $dates = ['created_at'];

    public function Origin(): BelongsTo
    {
        return $this->belongsTo(AviaAirport::class, 'code', 'origin_code');
    }

    public function Destination(): BelongsTo
    {
        return $this->belongsTo(AviaAirport::class, 'code', 'destination_code');
    }

}
