<?php

namespace App\Models\Avia;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Avia\AviaAlliance
 *
 * @property string $name название альянса
 * @property array $airlines коды компаний, входящих в альянс
 * @property Carbon $created_at
 * @method static Builder|AviaAlliance newModelQuery()
 * @method static Builder|AviaAlliance newQuery()
 * @method static Builder|AviaAlliance query()
 * @method static Builder|AviaAlliance whereAirlines($value)
 * @method static Builder|AviaAlliance whereCreatedAt($value)
 * @method static Builder|AviaAlliance whereName($value)
 * @mixin \Eloquent
 */
class AviaAlliance extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = ['created_at'];
    protected $casts = ['airlines' => 'array'];
    protected $primaryKey = 'md';

    public function getIncrementing(): bool
    {
        return false;
    }

    public function getKeyType(): string
    {
        return 'string';
    }
}
