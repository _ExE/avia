<?php

namespace App\Models\Avia;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Avia\AviaCountry
 *
 * @property string $code IATA код страны
 * @property string $name_en название страны EN
 * @property string $name_ru название страны RU
 * @property string $currency валюта страны
 * @property mixed $cases падежи
 * @property Carbon $created_at
 * @property-read Collection|AviaAirport[] $Airports
 * @property-read int|null $airports_count
 * @property-read Collection|AviaCity[] $Cities
 * @property-read int|null $cities_count
 * @method static Builder|AviaCountry newModelQuery()
 * @method static Builder|AviaCountry newQuery()
 * @method static Builder|AviaCountry query()
 * @method static Builder|AviaCountry whereCases($value)
 * @method static Builder|AviaCountry whereCode($value)
 * @method static Builder|AviaCountry whereCreatedAt($value)
 * @method static Builder|AviaCountry whereCurrency($value)
 * @method static Builder|AviaCountry whereNameEn($value)
 * @method static Builder|AviaCountry whereNameRu($value)
 * @mixin \Eloquent
 */
class AviaCountry extends Model
{
    protected $primaryKey = 'code';
    protected $guarded = [];
    protected $table = 'avia_countries';
    public $timestamps = false;
    protected $dates = ['created_at'];
    protected $casts = ['cases' => 'array'];

    public function getIncrementing(): bool
    {
        return false;
    }

    public function getKeyType(): string
    {
        return 'string';
    }

    public function Airports(): HasMany
    {
        return $this->hasMany(AviaAirport::class, 'country_code', 'code');
    }

    public function Cities(): HasMany
    {
        return $this->hasMany(AviaCity::class, 'country_code', 'code');
    }
}
