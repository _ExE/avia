<?php

namespace App\Jobs\Avia;

use App\Services\Avia\Update\Handlers\CityDirectionsHandler;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Throwable;

class CityDirectionsUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $origin;

    /**
     * Create a new job instance.
     *
     * @param string $origin
     */
    public function __construct(string $origin)
    {
        $this->origin = $origin;
    }

    /**
     * Execute the job.
     *
     * @param CityDirectionsHandler $cityDirectionsUpdater
     * @return void
     */
    public function handle(CityDirectionsHandler $cityDirectionsUpdater): void
    {
        try {
            $cityDirectionsUpdater->setOrigin($this->origin)->update();
        } catch (Throwable $e) {
            logError($e);
        }
    }

}
