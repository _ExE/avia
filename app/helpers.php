<?php

if (!function_exists('portRoute')) {
    /**
     * Generate the URL include port to a named route.
     *
     * @param array|string $name
     * @param mixed $parameters
     * @param bool $absolute
     * @return string
     */
    function portRoute($name, $parameters = [], $absolute = true)
    {
        $port = env('APP_PORT', null);
        $route = app('url')->route($name, $parameters, $absolute);
        if ($port === null) {
            return $route;
        }
        return str_replace(env('APP_URL'), env('APP_URL') . ":$port", $route);
    }
}

if (!function_exists('logError')) {
    /**
     * @param Throwable $e
     */
    function logError(Throwable $e)
    {
        Log::error($e->getMessage());
        Log::error( $e->getCode() . ' | ' . $e->getFile() . ' : ' . $e->getLine());
    }
}

if (!function_exists('jsonEncode')) {
    /**
     * @param array $data
     * @return string|null
     */
    function jsonEncode(array $data): ?string
    {
        try {
            $result = json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE);
        } catch (JsonException $e) {
            return null;
        }
        return $result === false ? null : $result;
    }
}

if (!function_exists('jsonDecode')) {
    /**
     * @param string $data
     * @return array|null
     */
    function jsonDecode(string $data): ?array
    {
        try {
            $result =  json_decode($data, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            return null;
        }
        return $result === false ? null : $result;
    }
}


