<?php

namespace App\Services\Avia\Update;


use App\Enums\Avia\DataType;
use App\Models\Avia\AviaUpdateResult;
use Throwable;

abstract class AbstractHandler
{
    protected AbstractDownloader $downloader;
    protected AbstractInserter $inserter;
    protected ResultLogger $logger;

    /**
     * AbstractUpdater constructor.
     * @param AbstractDownloader $downloader
     * @param AbstractInserter $inserter
     */
    public function __construct(AbstractDownloader $downloader, AbstractInserter $inserter)
    {
        $this->downloader = $downloader;
        $this->inserter = $inserter;
    }

    /**
     * @return AviaUpdateResult
     */
    public function update(): AviaUpdateResult
    {
        $this->logger = new ResultLogger($this->getUrl(), $this->getDataType());
        try {
            $downloadData = $this->downloader->do();
            $insertCount = $this->inserter->do($downloadData);
            $this->logger->okResult($downloadData->count(), $insertCount);
        } catch (Throwable $e) {
            $this->logger->errorResult($e);
        }
        $this->logger->setWarnings($this->downloader->getWarnings());
        return $this->logger->getResult();
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->downloader->getUrl();
    }

    /**
     * @return DataType
     */
    abstract public function getDataType(): DataType;

}
