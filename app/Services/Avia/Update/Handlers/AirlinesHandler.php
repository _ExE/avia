<?php

namespace App\Services\Avia\Update\Handlers;


use App\Enums\Avia\DataType;
use App\Services\Avia\Update\AbstractHandler;
use App\Services\Avia\Update\Downloaders\AirlinesDownloader;
use App\Services\Avia\Update\Inserters\AirlinesInserter;

class AirlinesHandler extends AbstractHandler
{
    /**
     * AirlinesHandler constructor.
     */
    public function __construct()
    {
        parent::__construct(new AirlinesDownloader(),new AirlinesInserter());
    }

    /**
     * @return DataType
     */
    public function getDataType(): DataType
    {
        return DataType::AIRLINES();
    }

}

