<?php

namespace App\Services\Avia\Update\Handlers;


use App\Enums\Avia\DataType;
use App\Services\Avia\Update\AbstractHandler;
use App\Services\Avia\Update\Downloaders\CityDirectionsDownloader;
use App\Services\Avia\Update\Inserters\CityDirectionsInserter;

class CityDirectionsHandler extends AbstractHandler
{
    /**
     * CityDirectionsHandler constructor.
     */
    public function __construct()
    {
        parent::__construct(new CityDirectionsDownloader(), new CityDirectionsInserter());
    }

    /**
     * @param string $origin
     * @return $this
     */
    public function setOrigin(string $origin): self
    {
        $this->downloader->setOrigin($origin);
        $this->inserter->setOrigin($origin);
        return $this;
    }

    /**
     * @return DataType
     */
    public function getDataType(): DataType
    {
        return DataType::CITY_DIRECTIONS();
    }

}

