<?php

namespace App\Services\Avia\Update\Handlers;


use App\Enums\Avia\DataType;
use App\Services\Avia\Update\AbstractHandler;
use App\Services\Avia\Update\Downloaders\CountriesDownloader;
use App\Services\Avia\Update\Inserters\CountriesInserter;

class CountriesHandler extends AbstractHandler
{
    /**
     * CountriesHandler constructor.
     */
    public function __construct()
    {
        parent::__construct(new CountriesDownloader(),new CountriesInserter());
    }

    /**
     * @return DataType
     */
    public function getDataType(): DataType
    {
        return DataType::COUNTRIES();
    }

}

