<?php

namespace App\Services\Avia\Update\Handlers;

use App\Enums\Avia\DataType;
use App\Services\Avia\Update\AbstractHandler;
use App\Services\Avia\Update\Downloaders\RoutesDownloader;
use App\Services\Avia\Update\Inserters\RoutesInserter;

class RoutesHandler extends AbstractHandler
{
    /**
     * PlanesHandler constructor.
     */
    public function __construct()
    {
        parent::__construct(new RoutesDownloader(),new RoutesInserter());
    }

    /**
     * @return DataType
     */
    public function getDataType(): DataType
    {
        return DataType::ROUTES();
    }

}

