<?php

namespace App\Services\Avia\Update\Handlers;


use App\Enums\Avia\DataType;
use App\Services\Avia\Update\AbstractHandler;
use App\Services\Avia\Update\Downloaders\CitiesDownloader;
use App\Services\Avia\Update\Inserters\CitiesInserter;

class CitiesHandler extends AbstractHandler
{
    /**
     * CitiesHandler constructor.
     */
    public function __construct()
    {
        parent::__construct(new CitiesDownloader(),new CitiesInserter());
    }

    /**
     * @return DataType
     */
    public function getDataType(): DataType
    {
        return DataType::CITIES();
    }

}

