<?php

namespace App\Services\Avia\Update\Handlers;


use App\Enums\Avia\DataType;
use App\Services\Avia\Update\AbstractHandler;
use App\Services\Avia\Update\Downloaders\PlanesDownloader;
use App\Services\Avia\Update\Inserters\PlanesInserter;

class PlanesHandler extends AbstractHandler
{
    /**
     * PlanesHandler constructor.
     */
    public function __construct()
    {
        parent::__construct(new PlanesDownloader(),new PlanesInserter());
    }

    /**
     * @return DataType
     */
    public function getDataType(): DataType
    {
        return DataType::PLANES();
    }

}

