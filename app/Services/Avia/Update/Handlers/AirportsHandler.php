<?php

namespace App\Services\Avia\Update\Handlers;


use App\Enums\Avia\DataType;
use App\Services\Avia\Update\AbstractHandler;
use App\Services\Avia\Update\Downloaders\AirportsDownloader;
use App\Services\Avia\Update\Inserters\AirportsInserter;

class AirportsHandler extends AbstractHandler
{
    /**
     * AirportsHandler constructor.
     */
    public function __construct()
    {
        parent::__construct(new AirportsDownloader(),new AirportsInserter());
    }

    /**
     * @return DataType
     */
    public function getDataType(): DataType
    {
        return DataType::AIRPORTS();
    }

}

