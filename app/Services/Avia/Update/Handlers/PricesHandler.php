<?php

namespace App\Services\Avia\Update\Handlers;


use App\Enums\Avia\DataType;
use App\Services\Avia\Update\AbstractHandler;
use App\Services\Avia\Update\Downloaders\PricesDownloader;
use App\Services\Avia\Update\Inserters\PricesInserter;

class PricesHandler extends AbstractHandler
{
    /**
     * PricesHandler constructor.
     */
    public function __construct()
    {
        parent::__construct(new PricesDownloader(), new PricesInserter());
    }

    /**
     * @param string $origin
     * @return $this
     */
    public function setOrigin(string $origin): self
    {
        $this->downloader->setOrigin($origin);
        $this->inserter->setOrigin($origin);
        return $this;
    }

    /**
     * @param string $destination
     * @return $this
     */
    public function setDestination(string $destination): self
    {
        $this->downloader->setDestination($destination);
        $this->inserter->setDestination($destination);
        return $this;
    }

    /**
     * @param string $destination
     * @return $this
     */
    public function setPage(int $page): self
    {
        $this->downloader->setPage($page);
        $this->inserter->setPage($page);
        return $this;
    }

    /**
     * @return DataType
     */
    public function getDataType(): DataType
    {
        return DataType::PRICES();
    }

}

