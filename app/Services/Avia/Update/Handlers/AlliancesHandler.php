<?php

namespace App\Services\Avia\Update\Handlers;


use App\Enums\Avia\DataType;
use App\Services\Avia\Update\AbstractHandler;
use App\Services\Avia\Update\Downloaders\AlliancesDownloader;
use App\Services\Avia\Update\Inserters\AlliancesInserter;

class AlliancesHandler extends AbstractHandler
{
    /**
     * AlliancesHandler constructor.
     */
    public function __construct()
    {
        parent::__construct(new AlliancesDownloader(),new AlliancesInserter());
    }

    /**
     * @return DataType
     */
    public function getDataType(): DataType
    {
        return DataType::ALLIANCES();
    }

}

