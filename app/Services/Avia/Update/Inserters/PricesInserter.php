<?php

namespace App\Services\Avia\Update\Inserters;


use App\Models\Avia\AviaPrice;
use App\Services\Avia\Update\AbstractInserter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class PricesInserter extends AbstractInserter
{
    private ?string $origin = null;
    private ?string $destination = null;
    private int $page = 1;

    /**
     * @param string $origin
     * @return $this
     */
    public function setOrigin(string $origin): self
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * @param string $destination
     * @return $this
     */
    public function setDestination(string $destination): self
    {
        $this->destination = $destination;
        return $this;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage(int $page): self
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return Model
     */
    protected function getModel(): Model
    {
        return AviaPrice::getModel();
    }

    /**
     * @inheritDoc
     */
    public function do(Collection $data): int
    {
        $params = [];
        if (null !== $this->origin) {
            $params['origin_code'] = $this->origin;
        }
        if (null !== $this->destination) {
            $params['destination_code'] = $this->destination;
        }
        if ($this->page > 1) {
            return $this->chunkInsert($data);
        }
        return $this->deleteAndInsert($data, $params);
    }

}

