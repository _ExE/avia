<?php

namespace App\Services\Avia\Update\Inserters;


use App\Models\Avia\AviaCityDirection;
use App\Services\Avia\Update\AbstractInserter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class CityDirectionsInserter extends AbstractInserter
{
    private string $origin = 'LED';

    /**
     * @param string $origin
     * @return $this
     */
    public function setOrigin(string $origin): self
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * @return Model
     */
    protected function getModel(): Model
    {
        return AviaCityDirection::getModel();
    }

    /**
     * @inheritDoc
     */
    public function do(Collection $data): int
    {
        return $this->deleteAndInsert($data, ['origin_code' => $this->origin]);
    }

}

