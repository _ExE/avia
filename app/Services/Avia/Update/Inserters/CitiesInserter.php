<?php

namespace App\Services\Avia\Update\Inserters;


use App\Models\Avia\AviaCity;
use App\Services\Avia\Update\AbstractInserter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class CitiesInserter extends AbstractInserter
{
    /**
     * @return Model
     */
    protected function getModel(): Model
    {
        return AviaCity::getModel();
    }

    /**
     * @inheritDoc
     */
    public function do(Collection $data): int
    {
        return $this->truncateAndInsert($data);
    }

}

