<?php

namespace App\Services\Avia\Update\Inserters;


use App\Models\Avia\AviaAlliance;
use App\Services\Avia\Update\AbstractInserter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class AlliancesInserter extends AbstractInserter
{
    /**
     * @return Model
     */
    protected function getModel(): Model
    {
        return AviaAlliance::getModel();
    }

    /**
     * @inheritDoc
     */
    public function do(Collection $data): int
    {
        return $this->truncateAndInsert($data);
    }

}

