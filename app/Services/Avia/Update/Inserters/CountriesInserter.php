<?php

namespace App\Services\Avia\Update\Inserters;


use App\Models\Avia\AviaCountry;
use App\Services\Avia\Update\AbstractInserter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class CountriesInserter extends AbstractInserter
{
    /**
     * @return Model
     */
    protected function getModel(): Model
    {
        return AviaCountry::getModel();
    }

    /**
     * @inheritDoc
     */
    public function do(Collection $data): int
    {
        return $this->truncateAndInsert($data);
    }

}

