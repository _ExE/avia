<?php


namespace App\Services\Avia\Update;

use App\Enums\Avia\DataType;
use App\Models\Avia\AviaUpdateResult;
use Throwable;

class ResultLogger
{
    protected AviaUpdateResult $updateResult;

    /**
     * ResultLogger constructor.
     * @param string $url
     * @param DataType $dataType
     */
    public function __construct(string $url, DataType $dataType)
    {
        $this->updateResult = AviaUpdateResult::firstOrCreate([
            'md' => md5($url),
        ], [
            'url' => $url,
        ]);
        $this->updateResult->data_type = $dataType->value();
        $this->updateResult->loaded = 0;
        $this->updateResult->inserted = 0;
        $this->updateResult->error = null;
        $this->updateResult->warnings = null;
    }

    /**
     * @param int $loaded
     * @param int $insert
     * @return $this
     */
    public function okResult(int $loaded, int $insert): self
    {
        $this->updateResult->update([
            'loaded' => $loaded,
            'inserted' => $insert,
            'error_count' => 0,
            'updated_at' => now(),
        ]);
        return $this;
    }

    /**
     * @param Throwable $e
     * @return $this
     */
    public function errorResult(Throwable $e): self
    {
        $this->updateResult->update([
            'error' => [
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTraceAsString(),
            ],
            'error_count' => $this->updateResult->error_count + 1,
        ]);
        return $this;
    }

    /**
     * @param array $warnings
     * @return $this
     */
    public function setWarnings(array $warnings): self
    {
        if (count($warnings) > 0) {
            $this->updateResult->update(['warnings' => $warnings]);
        }
        return $this;
    }

    /**
     * @return AviaUpdateResult
     */
    public function getResult(): AviaUpdateResult
    {
        return $this->updateResult;
    }

}
