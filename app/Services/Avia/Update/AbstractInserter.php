<?php


namespace App\Services\Avia\Update;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class AbstractInserter
{
    protected const SIZE_INSERT_CHUNK = 3000;

    /**
     * @return Model
     */
    abstract protected function getModel(): Model;

    /**
     * @param Collection $data
     * @return int
     */
    abstract public function do(Collection $data): int;

    /**
     * @param Collection $data
     * @return int
     */
    protected function truncateAndInsert(Collection $data): int
    {
        $this->getModel()::query()->truncate();
        return $this->chunkInsert($data);
    }

    /**
     * @param Collection $data
     * @param array $keys
     * @return int
     */
    protected function deleteAndInsert(Collection $data, array $keys): int
    {
        $query = $this->getModel()::query();
        if (empty($keys)) {
            $query->truncate();
        } else {
            foreach ($keys as $k => $v) {
                $query = $query->where($k, $v);
            }
            $query->delete();
        }
        return $this->chunkInsert($data);
    }

    /**
     * @param Collection $data
     * @return mixed
     */
    protected function chunkInsert(Collection $data)
    {
        return $data->chunk(static::SIZE_INSERT_CHUNK)->map(function (Collection $chunk) {
            return $this->getModel()::insertOrIgnore($chunk->toArray());
        })->sum();
    }

}
