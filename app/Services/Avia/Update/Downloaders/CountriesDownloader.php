<?php

namespace App\Services\Avia\Update\Downloaders;


use App\Enums\Avia\Language;
use App\Services\Avia\Update\AbstractDownloader;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator as ValidatorFacade;
use Illuminate\Validation\Validator;

class CountriesDownloader extends AbstractDownloader
{
    /**
     * @inheritDoc
     */
    public function getUrl(): string
    {
        return self::API_URL . 'data/' . Language::RU . '/countries.json';
    }

    /**
     * @inheritDoc
     */
    protected function validator(array $data): Validator
    {
        return ValidatorFacade::make($data, [
            'code' => 'required|alpha|size:2',
            'currency' => 'nullable|alpha|size:3',
            'name' => 'required|string',
            'name_translations.en' => 'required|string',
            'cases' => 'nullable|array',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function push(Collection $result, array $data): void
    {
        $result->push([
            'code' => $data['code'],
            'name_ru' => $data['name'],
            'name_en' => $data['name_translations']['en'],
            'currency' => $data['currency'],
            'cases' => jsonEncode($data['cases']),
            'created_at' => now(),
        ]);
    }
}

