<?php

namespace App\Services\Avia\Update\Downloaders;


use App\Enums\Avia\IataType;
use App\Enums\Avia\Language;
use App\Services\Avia\Update\AbstractDownloader;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator as ValidatorFacade;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class AirportsDownloader extends AbstractDownloader
{
    /**
     * @inheritDoc
     */
    public function getUrl(): string
    {
        return self::API_URL . 'data/' . Language::RU . '/airports.json';
    }

    /**
     * @inheritDoc
     */
    protected function validator(array $data): Validator
    {
        return ValidatorFacade::make($data, [
            'code' => 'required|alpha|size:3',
            'country_code' => 'required|alpha|size:2',
            'city_code' => 'nullable|alpha|size:3',
//            'iata_type' => ['required', new IsIataType()],
            'iata_type' => ['required', Rule::in(IataType::values())],
            'flightable' => 'required|boolean',
            'name' => 'nullable|string',
            'name_translations.en' => 'required|string',
            'time_zone' => 'nullable|string',
            'coordinates_lon' => 'nullable|numeric',
            'coordinates_lat' => 'nullable|numeric',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function push(Collection $result, array $data): void
    {
        $result->push([
            'code' => $data['code'],
            'country_code' => $data['country_code'],
            'city_code' => $data['city_code'] ?? null,
            'iata_type' => $data['iata_type'],
            'flightable' => $data['flightable'],
            'name_ru' => $data['name'] ?? $data['name_translations']['en'],
            'name_en' => $data['name_translations']['en'],
            'time_zone' => $data['time_zone'] ?? null,
            'coordinates_lon' => $data['coordinates']['lon'] ?? null,
            'coordinates_lat' => $data['coordinates']['lat'] ?? null,
            'created_at' => now(),
        ]);
    }
}

