<?php

namespace App\Services\Avia\Update\Downloaders;


use App\Services\Avia\Update\AbstractDownloader;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator as ValidatorFacade;
use Illuminate\Validation\Validator;

class PlanesDownloader extends AbstractDownloader
{
    /**
     * @inheritDoc
     */
    public function getUrl(): string
    {
        return self::API_URL . 'data/planes.json';
    }

    /**
     * @inheritDoc
     */
    protected function validator(array $data): Validator
    {
        return ValidatorFacade::make($data, [
            'code' => 'required|alpha_num|size:3',
            'name' => 'required|string',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function push(Collection $result, array $data): void
    {
        $result->push([
            'code' => $data['code'],
            'name' => $data['name'],
            'created_at' => now(),
        ]);
    }
}

