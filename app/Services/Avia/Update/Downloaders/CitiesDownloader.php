<?php

namespace App\Services\Avia\Update\Downloaders;


use App\Enums\Avia\Language;
use App\Services\Avia\Update\AbstractDownloader;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator as ValidatorFacade;
use Illuminate\Validation\Validator;

class CitiesDownloader extends AbstractDownloader
{
    /**
     * @inheritDoc
     */
    public function getUrl(): string
    {
        return self::API_URL . 'data/' . Language::RU . '/cities.json';
    }

    /**
     * @inheritDoc
     */
    protected function validator(array $data): Validator
    {
        return ValidatorFacade::make($data, [
            'code' => 'required|alpha|size:3',
            'country_code' => 'required|alpha|size:2',
            'name' => 'nullable|string',
            'name_translations.en' => 'required|string',
            'time_zone' => 'nullable|string',
            'coordinates_lon' => 'nullable|numeric',
            'coordinates_lat' => 'nullable|numeric',
            'cases' => 'nullable|array',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function push(Collection $result, array $data): void
    {
        $result->push([
            'code' => $data['code'],
            'country_code' => $data['country_code'],
            'name_ru' => $data['name'],
            'name_en' => $data['name_translations']['en'],
            'time_zone' => $data['time_zone'],
            'coordinates_lon' => $data['coordinates']['lon'] ?? null,
            'coordinates_lat' => $data['coordinates']['lat'] ?? null,
            'cases' => jsonEncode($data['cases']),
            'created_at' => now(),
        ]);
    }
}

