<?php

namespace App\Services\Avia\Update\Downloaders;


use App\Enums\Avia\Language;
use App\Services\Avia\Update\AbstractDownloader;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator as ValidatorFacade;
use Illuminate\Validation\Validator;

class AirlinesDownloader extends AbstractDownloader
{
    /**
     * @inheritDoc
     */
    public function getUrl(): string
    {
        return self::API_URL . 'data/' . Language::RU . '/airlines.json';
    }

    /**
     * @inheritDoc
     */
    protected function validator(array $data): Validator
    {
        return ValidatorFacade::make($data, [
            'code' => 'required|alpha_num|size:2',
            'name' => 'nullable|string',
            'name_translations.en' => 'required|string',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function push(Collection $result, array $data): void
    {
        $result->push([
            'code' => $data['code'],
            'name_ru' => $data['name'] ?? $data['name_translations']['en'],
            'name_en' => $data['name_translations']['en'],
            'created_at' => now(),
        ]);
    }
}

