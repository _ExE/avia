<?php

namespace App\Services\Avia\Update\Downloaders;

use App\Services\Avia\Update\AbstractDownloader;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator as ValidatorFacade;
use Illuminate\Validation\Validator;

class RoutesDownloader extends AbstractDownloader
{
    /**
     * @inheritDoc
     */
    public function getUrl(): string
    {
        return self::API_URL . 'data/routes.json';
    }

    /**
     * @inheritDoc
     */
    protected function validator(array $data): Validator
    {
        return ValidatorFacade::make($data, [
            'airline_iata' => 'nullable|alpha_num|size:2',
            'airline_icao' => 'nullable|alpha|size:3',
            'departure_airport_iata' => 'nullable|alpha|size:3',
            'departure_airport_icao' => 'nullable|alpha|size:3',
            'arrival_airport_iata' => 'nullable|alpha|size:3',
            'arrival_airport_icao' => 'nullable|alpha|size:3',
            'codeshare' => 'required|boolean',
            'transfers' => 'required|numeric',
            'planes' => 'nullable|array',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function push(Collection $result, array $data): void
    {
        $result->push([
            'airline_iata' => $data['airline_iata'],
            'departure_airport_iata' => $data['departure_airport_iata'],
            'arrival_airport_iata' => $data['arrival_airport_iata'],
            'airline_icao' => $data['airline_icao'],
            'departure_airport_icao' => $data['departure_airport_icao'],
            'arrival_airport_icao' => $data['arrival_airport_icao'],
            'codeshare' => $data['codeshare'],
            'transfers' => $data['transfers'],
            'planes' => jsonEncode($data['planes']),
            'created_at' => now(),
        ]);
    }
}

