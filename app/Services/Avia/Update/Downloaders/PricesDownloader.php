<?php

namespace App\Services\Avia\Update\Downloaders;


use App\Exceptions\Avia\UpdateException;
use App\Services\Avia\Update\AbstractDownloader;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator as ValidatorFacade;
use Illuminate\Support\Str;
use Illuminate\Validation\Validator;

class PricesDownloader extends AbstractDownloader
{
    private ?string $origin = null;
    private ?string $destination = null;
    private int $page = 1;

    /**
     * @param string $origin
     * @return $this
     */
    public function setOrigin(string $origin): self
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * @param string $destination
     * @return $this
     */
    public function setDestination(string $destination): self
    {
        $this->destination = $destination;
        return $this;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage(int $page): self
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUrl(): string
    {
        $url = self::API_URL .
            'v2/prices/latest?';
        if (null !== $this->origin) {
            $url .= 'origin=' . $this->origin;           //аэропорт вылета
        }
        if (null !== $this->destination) {
            $url .= '&destination=' . $this->destination; //аэропорт прилета
        }
        $url .=
            '&period_type=month' .          //Период, за который были найдены билеты (необходимый параметр)
            /*
             * Начало периода, в пределах которого выпадают даты вылета (в формате гггг-ММ-ДД,
             * например, 2016-05-01). Необходимо указать, если period_type равен месяцу.
             */
            '&beginning_of_period=' . Carbon::now()->startOfMonth()->toDateString().
            '&one_way=false' .              //true - в одну сторону, false - туда-обратно.
            '&page=' . $this->page .        //номер страницы
            '&limit=1000' .                 //количество записей на странице, максимум 1000
            /*
             * false-все цены, true-только цены, найденные с помощью маркера партнера (рекомендуется).
             */
            '&show_to_affiliates=true' .
            /*
             * Сортировка цен: price - по цене.
             * Для направлений возможна только сортировка city - город по цене;
             * route - по популярности маршрута; distance_unit_price - по цене за 1 км.
             */
            '&sorting=price' .
            '&currency=rub' .               //валюта
            '&trip_class=0' .                //Класс полета: 0-эконом-класс, 1 — Бизнес — класс, 2-первый класс.
            '&token=' . env('TRAVEL_TOKEN');
        //'trip_duration=1&' .          //продолжительность? не понятно
        return $url;
    }

    /**
     * @inheritDoc
     */
    protected function getResponse(): array
    {
        $response = $this->doRequest();
        if (false === $response['success']) {
            throw new UpdateException($response['message']);
        }
        if (isset($response['data']['errors'])) {
            throw new UpdateException(print_r($response['data']['errors'], true));
        }
        return $response['data'];
    }

    /**
     * @inheritDoc
     */
    protected function validator(array $data): Validator
    {
        return ValidatorFacade::make($data, [
            'value' => 'required|numeric',
            'trip_class' => 'required|numeric',
            'show_to_affiliates' => 'required|boolean',
            'return_date' => 'required|date',
            'origin' => 'required|alpha|size:3',
            'number_of_changes' => 'required|numeric',
            'gate' => 'required|string',
            'found_at' => ' required|date',
            'duration' => ' required|numeric',
            'distance' => ' required|numeric',
            'destination' => 'required|alpha|size:3',
            'depart_date' => ' required|date',
            'actual' => 'required|boolean',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function push(Collection $result, array $data): void
    {
        $result->push([
            'id' => (string)Str::uuid(),
            'origin_code' => $data['origin'],
            'destination_code' => $data['destination'],
            'show_to_affiliates' => $data['show_to_affiliates'],
            'trip_class' => $data['trip_class'],
            'depart_date' => new Carbon($data['depart_date']),
            'return_date' => new Carbon($data['return_date']),
            'number_of_changes' => $data['number_of_changes'],
            'value' => $data['value'],
            'found_at' => new Carbon($data['found_at']),
            'distance' => $data['distance'],
            'actual' => $data['actual'],
            'gate' => $data['gate'],
            'created_at' => now(),
        ]);
    }
}
