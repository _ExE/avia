<?php

namespace App\Services\Avia\Update\Downloaders;


use App\Enums\Avia\Language;
use App\Services\Avia\Update\AbstractDownloader;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator as ValidatorFacade;
use Illuminate\Validation\Validator;

class AlliancesDownloader extends AbstractDownloader
{
    /**
     * @inheritDoc
     */
    public function getUrl(): string
    {
        return self::API_URL . 'data/' . Language::RU . '/alliances.json';
    }

    /**
     * @inheritDoc
     */
    protected function validator(array $data): Validator
    {
        return ValidatorFacade::make($data, [
            'name' => 'required|string',
            'airlines' => 'required|array',
            'airlines.*' => 'required|alpha_num|size:2',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function push(Collection $result, array $data): void
    {
        $result->push([
            'name' => $data['name'],
            'airlines' => jsonEncode($data['airlines']),
            'created_at' => now(),
        ]);
    }
}

