<?php

namespace App\Services\Avia\Update\Downloaders;


use App\Exceptions\Avia\UpdateException;
use App\Services\Avia\Update\AbstractDownloader;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator as ValidatorFacade;
use Illuminate\Support\Str;
use Illuminate\Validation\Validator;

class CityDirectionsDownloader extends AbstractDownloader
{
    private string $origin = 'LED';

    /**
     * @param string $origin
     * @return $this
     */
    public function setOrigin(string $origin): self
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUrl(): string
    {
        return self::API_URL .
            'v1/city-directions?origin=' . $this->origin . '&currency=rub&token=' . env('TRAVEL_TOKEN');
    }

    /**
     * @inheritDoc
     */
    protected function getResponse(): array
    {
        $response = $this->doRequest();
        if (false === $response['success']) {
            throw new UpdateException($response['message']);
        }
        return $response['data'];
    }

    /**
     * @inheritDoc
     */
    protected function validator(array $data): Validator
    {
        return ValidatorFacade::make($data, [
            'origin' => 'required|alpha|size:3',
            'destination' => 'required|alpha|size:3',
            'price' => 'required|numeric',
            'transfers' => 'required|numeric',
            'airline' => 'required|alpha_num|size:2',
            'flight_number' => 'required|numeric',
            'departure_at' => ' required|date',
            'return_at' => ' required|date',
            'expires_at' => ' required|date',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function push(Collection $result, array $data): void
    {
        $result->push([
            'id' => (string)Str::uuid(),
            'origin_code' => $data['origin'],
            'destination_code' => $data['destination'],
            'price' => $data['price'],
            'transfers' => $data['transfers'],
            'airline_code' => $data['airline'],
            'flight_number' => $data['flight_number'],
            'departure_at' => new Carbon($data['departure_at']),
            'return_at' => new Carbon($data['return_at']),
            'expires_at' => new Carbon($data['expires_at']),
            'created_at' => now(),
        ]);
    }
}
