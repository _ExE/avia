<?php


namespace App\Services\Avia\Update;


use App\Exceptions\Avia\UpdateException;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Validation\Validator;
use Throwable;

abstract class AbstractDownloader
{
    protected const API_URL = 'https://api.travelpayouts.com/';
    protected array $warnings = [];

    /**
     * @return array
     */
    public function getWarnings(): array
    {
        return $this->warnings;
    }

    /**
     * @return array
     * @throws UpdateException
     */
    protected function doRequest(): array
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $this->getUrl(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => 'gzip,deflate',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'x-access-token: ' . env('TRAVEL_TOKEN'),
            ],
        ]);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            throw new UpdateException($err);
        }
        return jsonDecode($response);
    }

    /**
     * @return array
     * @throws UpdateException
     */
    protected function getResponse(): array
    {
        return $this->doRequest();
    }

    /**
     * @param Collection $result
     * @return Collection
     * @throws UpdateException
     */
    protected function validateDownloadResult(Collection $result): Collection
    {
        if (0 === $result->count()) {
            throw new UpdateException('Loaded 0 items');
        }
        return $result;
    }

    /**
     * @return Collection
     * @throws UpdateException
     */
    public function do(): Collection
    {
        $result = new Collection();
        $response = $this->getResponse();
        foreach ($response as $data) {
            if (!$this->isValid($data)) {
                continue;
            }
            try {
                $this->push($result, $data);
            } catch (Throwable $e) {
                $this->warnings[] = ['data' => $data, 'push_error' => $e->getMessage()];
            }
        }
        return $this->validateDownloadResult($result);
    }

    /**
     * @param array $data
     * @return bool
     */
    protected function isValid(array $data): bool
    {
        $validator = $this->validator($data);
        if ($validator->fails()) {
            $this->warnings[] = ['data' => $data, 'validate_errors' => $validator->errors()];
            return false;
        }
        return true;
    }

    /**
     * @return string
     */
    abstract public function getUrl(): string;

    /**
     * @param Collection $result
     * @param array $data
     * @throws Exception
     */
    abstract public function push(Collection $result, array $data): void;

    /**
     * @param array $data
     * @return Validator
     */
    abstract protected function validator(array $data): Validator;

}
