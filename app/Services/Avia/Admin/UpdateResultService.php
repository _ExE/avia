<?php

namespace App\Services\Avia\Admin;


use App\Enums\Avia\DataType;
use App\Models\Avia\AviaUpdateResult;
use Illuminate\Support\Collection;

class UpdateResultService
{
    /**
     * @param int $limit
     * @return Collection
     */
    public function getLatestErrorsGroupByTypes(int $limit = 100): Collection
    {
        return $this->getErrorDataTypes()->map(function ($dataType) use ($limit) {
            return [$dataType => $this->getLatestErrors($limit, DataType::memberByValue($dataType))];
        })->collapse();
    }

    /**
     * @return Collection
     */
    public function getErrorDataTypes(): Collection
    {
        return AviaUpdateResult::select('data_type')
            ->whereNotNull('error')->distinct()->get()->pluck('data_type');
    }

    /**
     * @param int $limit
     * @param DataType|null $dataType
     * @return Collection
     */
    public function getLatestErrors(int $limit = 10, DataType $dataType = null): Collection
    {
        $query = AviaUpdateResult::whereNotNull('error')
            ->latest('updated_at')
            ->take($limit);
        if ($dataType !== null) {
            $query = $query->where('data_type', $dataType->value());
        }
        return $query->get();
    }

    /**
     * @param int $limit
     * @return Collection
     */
    public function getLatestWarningsGroupByTypes(int $limit = 10): Collection
    {
        return $this->getWarningDataTypes()->map(function ($dataType) use ($limit) {
            return [$dataType => $this->getLatestWarnings($limit, DataType::memberByValue($dataType))];
        })->collapse();
    }

    /**
     * @return Collection
     */
    public function getWarningDataTypes(): Collection
    {
        return AviaUpdateResult::select('data_type')
            ->whereNotNull('warnings')->distinct()->get()->pluck('data_type');
    }

    /**
     * @param int $limit
     * @param DataType|null $dataType
     * @return Collection
     */
    public function getLatestWarnings(int $limit = 10, DataType $dataType = null): Collection
    {
        $query = AviaUpdateResult::whereNotNull('warnings')
            ->latest('updated_at')
            ->take($limit);
        if ($dataType !== null) {
            $query = $query->where('data_type', $dataType->value());
        }
        return $query->get();
    }

    /**
     * @return Collection
     */
    public function getUpdateDates(): Collection
    {
        return AviaUpdateResult::select('data_type')
            ->selectRaw('MIN(updated_at) as minDate')
            ->selectRaw('MAX(updated_at) AS maxDate')
            ->selectRaw('SUM(error_count) AS errorCount')
            ->selectRaw('SUM(loaded) AS loaded')
            ->selectRaw('SUM(inserted) AS inserted')
            ->groupBy('data_type')
            ->get();
    }

}
