<?php

namespace Tests\Feature;

use Tests\TestCase;

class RoutsTest extends TestCase
{
    public function testHomeRout(): void
    {
        $name = 'home';
        $url = '/';
        $status = 200;

        $response1 = $this->get(route($name));
        $response1->assertStatus($status);
        $response2 = $this->get($url);
        $response2->assertStatus($status);
        $this->assertEquals($response1->content(), $response2->content());
    }

    public function testAdminHomeRout(): void
    {
        $name = 'admin.home';
        $url = '/sa/';
        $status = 302;

        $response1 = $this->get(route($name));
        $response1->assertStatus($status);
        $response2 = $this->get($url);
        $response2->assertStatus($status);
        $this->assertEquals($response1->content(), $response2->content());
    }

    public function testAviaHomeRout(): void
    {
        $name = 'avia.home';
        $url = 'http://avia.' . config('app.domain');
        $status = 200;

        $response1 = $this->get(route($name));
        $response1->assertStatus($status);
        $response2 = $this->get($url);
        $response2->assertStatus($status);
        $this->assertEquals($response1->content(), $response2->content());
    }

}
