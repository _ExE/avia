<?php

namespace Tests\Unit\Services\Avia\Downloaders;

use App\Exceptions\Avia\UpdateException;
use App\Services\Avia\Update\Downloaders\AirlinesDownloader;

class AirlinesTestDownloader extends TestDownloader
{

    public function testGetUrl(): void
    {
        $downloader = new AirlinesDownloader();
        $this->assertEquals(
            'https://api.travelpayouts.com/data/ru/airlines.json',
            $downloader->getUrl()
        );
    }

    /**
     * @throws UpdateException
     */
    public function testDo(): void
    {
        $doRequestStub = [
            [
                'name' => 'Южное Небо',
                'code' => 'IH',
                'name_translations' => [
                    'en' => 'Southern Sky',
                ],
            ],
            [
                'name' => null,
                'code' => 'Z7',
                'name_translations' => [
                    'en' => 'Amaszonas',
                ],
            ]
        ];

        $result = $this->getMockDoResult(AirlinesDownloader::class,$doRequestStub);
        $this->assertCount(2, $result);

        $firstResult = $result->first();
        $this->assertCount(4, $firstResult);
        $this->assertEquals('IH', $firstResult['code']);
        $this->assertEquals('Южное Небо', $firstResult['name_ru']);
        $this->assertEquals('Southern Sky', $firstResult['name_en']);
        $this->assertTrue(now()->diffInSeconds($firstResult['created_at']) < 1);
    }

}
