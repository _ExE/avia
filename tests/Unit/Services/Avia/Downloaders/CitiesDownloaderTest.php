<?php

namespace Tests\Unit\Services\Avia\Downloaders;

use App\Exceptions\Avia\UpdateException;
use App\Services\Avia\Update\Downloaders\CitiesDownloader;

class CitiesDownloaderTest extends TestDownloader
{

    public function testGetUrl(): void
    {
        $downloader = new CitiesDownloader();
        $this->assertEquals(
            'https://api.travelpayouts.com/data/ru/cities.json',
            $downloader->getUrl()
        );
    }

    /**
     * @throws UpdateException
     */
    public function testDo(): void
    {
        $doRequestStub = [
            [
                'time_zone' => 'America/New_York',
                'name' => 'Квакертаун',
                'coordinates' => [
                    'lon' => -75.35,
                    'lat' => 40.433334,
                ],
                'code' => 'UKT',
                'cases' => [
                    'vi' => '',
                    'tv' => '',
                    'ro' => '',
                    'pr' => '',
                    'da' => '',
                ],
                'name_translations' => [
                    'en' => 'Quakertown',
                ],
                'country_code' => 'US',
            ],
            [
                'time_zone' => 'Asia/Shanghai',
                'name' => 'Линьфэнь',
                'coordinates' => [
                    'lon' => 111.64158,
                    'lat' => 36.13222,
                ],
                'code' => 'LFQ',
                'cases' => [
                    'vi' => '',
                    'tv' => '',
                    'ro' => '',
                    'pr' => '',
                    'da' => '',
                ],
                'name_translations' => [
                    'en' => 'Linfen',
                ],
                'country_code' => 'CN',
            ],
        ];

        $result = $this->getMockDoResult(CitiesDownloader::class,$doRequestStub);
        $this->assertCount(2, $result);

        $firstResult = $result->first();
        $this->assertCount(9, $firstResult);
        $this->assertEquals('UKT', $firstResult['code']);
        $this->assertEquals('US', $firstResult['country_code']);
        $this->assertEquals('Квакертаун', $firstResult['name_ru']);
        $this->assertEquals('Quakertown', $firstResult['name_en']);
        $this->assertEquals(-75.35, $firstResult['coordinates_lon']);
        $this->assertEquals(40.433334, $firstResult['coordinates_lat']);
        $this->assertEquals('{"vi":"","tv":"","ro":"","pr":"","da":""}', $firstResult['cases']);
        $this->assertTrue(now()->diffInSeconds($firstResult['created_at']) < 1);
    }

}
