<?php

namespace Tests\Unit\Services\Avia\Downloaders;

use App\Exceptions\Avia\UpdateException;
use App\Services\Avia\Update\Downloaders\PricesDownloader;
use Carbon\Carbon;

class PricesDownloaderTest extends TestDownloader
{

    public function testGetUrl(): void
    {
        $downloader = new PricesDownloader();
        $this->assertEquals(
            'https://api.travelpayouts.com/v2/prices/latest?&period_type=month&beginning_of_period=' .
            Carbon::now()->startOfMonth()->toDateString() .
            '&one_way=false&page=1&limit=1000&show_to_affiliates=true&sorting=price&currency=rub&trip_class=0&token=' .
            env('TRAVEL_TOKEN'),
            $downloader->getUrl()
        );
    }

    /**
     * @throws UpdateException
     */
    public function testDo(): void
    {
        $doRequestStub = [
            'success' => true,
            'data' => [
                [
                    'value' => 619,
                    'trip_class' => 0,
                    'show_to_affiliates' => true,
                    'return_date' => '2020-10-31',
                    'origin' => 'BKK',
                    'number_of_changes' => 0,
                    'gate' => 'Trip.com',
                    'found_at' => '2020-10-14T08:19:58.131570',
                    'duration' => 165,
                    'distance' => 584,
                    'destination' => 'CNX',
                    'depart_date' => '2020-10-19',
                    'actual' => true,
                ],
                [
                    'value' => 873,
                    'trip_class' => 0,
                    'show_to_affiliates' => true,
                    'return_date' => '2020-10-31',
                    'origin' => 'PED',
                    'number_of_changes' => 0,
                    'gate' => 'Wizz Air',
                    'found_at' => '2020-10-17T03:41:49.911456',
                    'duration' => 185,
                    'distance' => 586,
                    'destination' => 'LWO',
                    'depart_date' => '2020-10-24',
                    'actual' => true,
                ],
            ],
        ];

        $result = $this->getMockDoResult(PricesDownloader::class,$doRequestStub);
        $this->assertCount(2, $result);

        $firstResult = $result->first();
        $this->assertCount(14, $firstResult);
        $this->assertEquals('BKK', $firstResult['origin_code']);
        $this->assertEquals('CNX', $firstResult['destination_code']);
        $this->assertTrue($firstResult['show_to_affiliates']);
        $this->assertEquals(0, $firstResult['trip_class']);
        $this->assertEquals(new Carbon('2020-10-19'), $firstResult['depart_date']);
        $this->assertEquals(new Carbon('2020-10-31'), $firstResult['return_date']);
        $this->assertEquals(0, $firstResult['number_of_changes']);
        $this->assertEquals(619, $firstResult['value']);
        $this->assertEquals(new Carbon('2020-10-14T08:19:58.131570'), $firstResult['found_at']);
        $this->assertEquals(584, $firstResult['distance']);
        $this->assertTrue($firstResult['actual']);
        $this->assertEquals('Trip.com', $firstResult['gate']);
        $this->assertTrue(now()->diffInSeconds($firstResult['created_at']) < 1);
    }

}
