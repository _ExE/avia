<?php

namespace Tests\Unit\Services\Avia\Downloaders;

use App\Exceptions\Avia\UpdateException;
use App\Services\Avia\Update\Downloaders\AlliancesDownloader;

class AlliancesDownloaderTest extends TestDownloader
{

    public function testGetUrl(): void
    {
        $downloader = new AlliancesDownloader();
        $this->assertEquals(
            'https://api.travelpayouts.com/data/ru/alliances.json',
            $downloader->getUrl()
        );
    }

    /**
     * @throws UpdateException
     */
    public function testDo(): void
    {
        $doRequestStub = [
            [
                'name' => 'Star Alliance',
                'name_translations' => [
                    'en' => 'Star Alliance',
                ],
                'airlines' => [
                    0 => 'CM',
                    1 => 'A3',
                    2 => 'AC',
                ],
            ],
            [
                'name' => 'OneWorld',
                'name_translations' => [
                    'en' => 'OneWorld',
                ],
                'airlines' => [
                    0 => 'CX',
                    1 => 'JL',
                    2 => 'BA',
                ],
            ],
        ];

        $result = $this->getMockDoResult(AlliancesDownloader::class,$doRequestStub);
        $this->assertCount(2, $result);

        $firstResult = $result->first();
        $this->assertCount(3, $firstResult);
        $this->assertEquals('Star Alliance', $firstResult['name']);
        $this->assertEquals('["CM","A3","AC"]', $firstResult['airlines']);
        $this->assertTrue(now()->diffInSeconds($firstResult['created_at']) < 1);
    }

}
