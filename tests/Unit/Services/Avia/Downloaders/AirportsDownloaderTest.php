<?php

namespace Tests\Unit\Services\Avia\Downloaders;

use App\Exceptions\Avia\UpdateException;
use App\Services\Avia\Update\Downloaders\AirportsDownloader;

class AirportsDownloaderTest extends TestDownloader
{

    public function testGetUrl(): void
    {
        $downloader = new AirportsDownloader();
        $this->assertEquals(
            'https://api.travelpayouts.com/data/ru/airports.json',
            $downloader->getUrl()
        );
    }

    /**
     * @throws UpdateException
     */
    public function testDo(): void
    {
        $doRequestStub = [
            [
                'time_zone' => 'America/Lima',
                'name' => 'Андауайлас',
                'iata_type' => 'airport',
                'flightable' => true,
                'coordinates' => [
                    'lon' => -73.355835,
                    'lat' => -13.716667,
                ],
                'code' => 'ANS',
                'name_translations' => [
                    'en' => 'Andahuaylas',
                ],
                'country_code' => 'PE',
                'city_code' => 'ANS',
            ],
            [
                'time_zone' => 'Asia/Shanghai',
                'name' => 'Линьфэнь',
                'iata_type' => 'airport',
                'flightable' => true,
                'coordinates' => [
                    'lon' => 111.64158,
                    'lat' => 36.13222,
                ],
                'code' => 'LFQ',
                'name_translations' => [
                    'en' => 'Linfen Qiaoli Airport',
                ],
                'country_code' => 'CN',
                'city_code' => 'LFQ',
            ],
        ];

        $result = $this->getMockDoResult(AirportsDownloader::class,$doRequestStub);
        $this->assertCount(2, $result);

        $firstResult = $result->first();
        $this->assertCount(11, $firstResult);
        $this->assertEquals('ANS', $firstResult['code']);
        $this->assertEquals('PE', $firstResult['country_code']);
        $this->assertEquals('ANS', $firstResult['city_code']);
        $this->assertTrue($firstResult['flightable']);
        $this->assertEquals('Андауайлас', $firstResult['name_ru']);
        $this->assertEquals('Andahuaylas', $firstResult['name_en']);
        $this->assertEquals('America/Lima', $firstResult['time_zone']);
        $this->assertEquals(-73.355835, $firstResult['coordinates_lon']);
        $this->assertEquals(-13.716667, $firstResult['coordinates_lat']);
        $this->assertTrue(now()->diffInSeconds($firstResult['created_at']) < 1);
    }

}
