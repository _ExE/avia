<?php

namespace Tests\Unit\Services\Avia\Downloaders;

use App\Exceptions\Avia\UpdateException;
use App\Services\Avia\Update\Downloaders\RoutesDownloader;

class RoutesDownloaderTest extends TestDownloader
{

    public function testGetUrl(): void
    {
        $downloader = new RoutesDownloader();
        $this->assertEquals(
            'https://api.travelpayouts.com/data/routes.json',
            $downloader->getUrl()
        );
    }

    /**
     * @throws UpdateException
     */
    public function testDo(): void
    {
        $doRequestStub = [
            [
                'airline_iata' => '2B',
                'airline_icao' => null,
                'departure_airport_iata' => 'AER',
                'departure_airport_icao' => null,
                'arrival_airport_iata' => 'DME',
                'arrival_airport_icao' => null,
                'codeshare' => false,
                'transfers' => 0,
                'planes' => [
                    0 => 'CR2',
                ],
            ],
            [
                'airline_iata' => '4O',
                'airline_icao' => null,
                'departure_airport_iata' => 'MEX',
                'departure_airport_icao' => null,
                'arrival_airport_iata' => 'OAX',
                'arrival_airport_icao' => null,
                'codeshare' => false,
                'transfers' => 0,
                'planes' => [
                    0 => '320',
                ],
            ],
        ];

        $result = $this->getMockDoResult(RoutesDownloader::class,$doRequestStub);
        $this->assertCount(2, $result);

        $firstResult = $result->first();
        $this->assertCount(10, $firstResult);
        $this->assertEquals('2B', $firstResult['airline_iata']);
        $this->assertEquals('AER', $firstResult['departure_airport_iata']);
        $this->assertEquals('DME', $firstResult['arrival_airport_iata']);
        $this->assertNull( $firstResult['airline_icao']);
        $this->assertNull( $firstResult['departure_airport_icao']);
        $this->assertNull( $firstResult['arrival_airport_icao']);
        $this->assertFalse( $firstResult['codeshare']);
        $this->assertEquals(0, $firstResult['transfers']);
        $this->assertEquals('["CR2"]', $firstResult['planes']);
        $this->assertTrue(now()->diffInSeconds($firstResult['created_at']) < 1);
    }

}
