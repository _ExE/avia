<?php

namespace Tests\Unit\Services\Avia\Downloaders;

use App\Exceptions\Avia\UpdateException;
use App\Services\Avia\Update\Downloaders\CountriesDownloader;

class CountriesDownloaderTest extends TestDownloader
{

    public function testGetUrl(): void
    {
        $downloader = new CountriesDownloader();
        $this->assertEquals(
            'https://api.travelpayouts.com/data/ru/countries.json',
            $downloader->getUrl()
        );
    }

    /**
     * @throws UpdateException
     */
    public function testDo(): void
    {
        $doRequestStub = [
            [
                'name' => 'Испания',
                'currency' => 'EUR',
                'code' => 'ES',
                'cases' => [
                    'vi' => 'в Испанию',
                    'tv' => 'Испанией',
                    'ro' => 'Испании',
                    'pr' => 'Испании',
                    'da' => 'Испании',
                ],
                'name_translations' => [
                    'en' => 'Spain',
                ],
            ],
            [
                'name' => 'Белиз',
                'currency' => 'BZD',
                'code' => 'BZ',
                'cases' => [
                    'vi' => 'в Белиз',
                    'tv' => 'Белизом',
                    'ro' => 'Белиза',
                    'pr' => 'Белизе',
                    'da' => 'Белизу',
                ],
                'name_translations' => [
                    'en' => 'Belize',
                ],
            ],
        ];

        $result = $this->getMockDoResult(CountriesDownloader::class,$doRequestStub);
        $this->assertCount(2, $result);

        $firstResult = $result->first();
        $this->assertCount(6, $firstResult);
        $this->assertEquals('ES', $firstResult['code']);
        $this->assertEquals('Испания', $firstResult['name_ru']);
        $this->assertEquals('Spain', $firstResult['name_en']);
        $this->assertEquals('EUR', $firstResult['currency']);
        $this->assertEquals('{"vi":"в Испанию","tv":"Испанией","ro":"Испании","pr":"Испании","da":"Испании"}', $firstResult['cases']);
        $this->assertTrue(now()->diffInSeconds($firstResult['created_at']) < 1);
    }

}
