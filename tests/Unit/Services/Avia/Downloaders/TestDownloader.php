<?php

namespace Tests\Unit\Services\Avia\Downloaders;

use App\Exceptions\Avia\UpdateException;
use App\Services\Avia\Update\AbstractDownloader;
use Illuminate\Support\Collection;
use Mockery;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class TestDownloader extends TestCase
{
    /**
     * @param string $className
     * @param array $doRequestStub
     * @return Collection
     * @throws UpdateException
     */
    public function getMockDoResult(string $className, array $doRequestStub): Collection
    {
        /** @var AbstractDownloader|MockObject $mock */
        $mock = $this->getMockBuilder($className)
            ->onlyMethods(['doRequest'])
            ->getMock();
        $mock->method('doRequest')->willReturn($doRequestStub);

        return $mock->do();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

}
