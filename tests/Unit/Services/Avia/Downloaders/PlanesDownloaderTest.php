<?php

namespace Tests\Unit\Services\Avia\Downloaders;

use App\Exceptions\Avia\UpdateException;
use App\Services\Avia\Update\Downloaders\PlanesDownloader;

class PlanesDownloaderTest extends TestDownloader
{

    public function testGetUrl(): void
    {
        $downloader = new PlanesDownloader();
        $this->assertEquals(
            'https://api.travelpayouts.com/data/planes.json',
            $downloader->getUrl()
        );
    }

    /**
     * @throws UpdateException
     */
    public function testDo(): void
    {
        $doRequestStub = [
            [
                'code' => '100',
                'name' => 'Fokker 100',
            ],
            [
                'code' => 'D9X',
                'name' => 'Boeing/McDonnell Douglas DC-9-10 Freighter',
            ],
        ];
        $result = $this->getMockDoResult(PlanesDownloader::class,$doRequestStub);
        $this->assertCount(2, $result);

        $firstResult = $result->first();
        $this->assertCount(3, $firstResult);
        $this->assertEquals('100', $firstResult['code']);
        $this->assertEquals('Fokker 100', $firstResult['name']);
        $this->assertTrue(now()->diffInSeconds($firstResult['created_at']) < 1);
    }

}
