<?php

namespace Tests\Unit\Services\Avia\Downloaders;

use App\Exceptions\Avia\UpdateException;
use App\Services\Avia\Update\Downloaders\CityDirectionsDownloader;
use Carbon\Carbon;

class CityDirectionsDownloaderTest extends TestDownloader
{

    public function testGetUrl(): void
    {
        $downloader = (new CityDirectionsDownloader())->setOrigin('LED');
        $this->assertEquals(
            'https://api.travelpayouts.com/v1/city-directions?origin=LED&currency=rub&token=' .
            env('TRAVEL_TOKEN'),
            $downloader->getUrl()
        );
    }

    /**
     * @throws UpdateException
     */
    public function testDo(): void
    {
        $doRequestStub = [
            'success' => true,
            'data' => [
                'AER' => [
                    'origin' => 'LED',
                    'destination' => 'AER',
                    'price' => 4482,
                    'transfers' => 1,
                    'airline' => 'U6',
                    'flight_number' => 7772,
                    'departure_at' => '2020-11-07T22:35:00Z',
                    'return_at' => '2020-11-20T16:00:00Z',
                    'expires_at' => '2020-10-17T09:26:29Z',
                ],
                'AYT' => [
                    'origin' => 'LED',
                    'destination' => 'AYT',
                    'price' => 9452,
                    'transfers' => 1,
                    'airline' => 'U6',
                    'flight_number' => 7772,
                    'departure_at' => '2021-01-30T22:35:00Z',
                    'return_at' => '2021-02-06T21:50:00Z',
                    'expires_at' => '2020-10-16T18:39:53Z',
                ]
            ],
        ];

        $result = $this->getMockDoResult(CityDirectionsDownloader::class,$doRequestStub);
        $this->assertCount(2, $result);

        $firstResult = $result->first();
        $this->assertCount(11, $firstResult);
        $this->assertEquals('LED', $firstResult['origin_code']);
        $this->assertEquals('AER', $firstResult['destination_code']);
        $this->assertEquals(4482, $firstResult['price']);
        $this->assertEquals(1, $firstResult['transfers']);
        $this->assertEquals('U6', $firstResult['airline_code']);
        $this->assertEquals(7772, $firstResult['flight_number']);
        $this->assertEquals(new Carbon('2020-11-07T22:35:00Z'), $firstResult['departure_at']);
        $this->assertEquals(new Carbon('2020-11-20T16:00:00Z'), $firstResult['return_at']);
        $this->assertEquals(new Carbon('2020-10-17T09:26:29Z'), $firstResult['expires_at']);
        $this->assertTrue(now()->diffInSeconds($firstResult['created_at']) < 1);
    }

}
