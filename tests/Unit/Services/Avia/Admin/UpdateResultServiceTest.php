<?php

namespace Tests\Unit\Services\Avia\Admin;

use App\Models\Avia\AviaUpdateResult;
use App\Services\Avia\Admin\UpdateResultService;
use Tests\TestCase;

class UpdateResultServiceTest extends TestCase
{
    public function testGetLatestErrors(): void
    {
        $count = 2;
        $result1 = AviaUpdateResult::factory()->withError()->count($count)->create()->reverse();
        $result2 = (new UpdateResultService())->getLatestErrors($count);

        $this->assertEquals($count,$result2->count());
        $this->assertEquals($result1->sort()->pluck('error'),$result2->sort()->pluck('error'));
    }

    public function testGetLatestWarnings(): void
    {
        $count = 2;
        $result1 = AviaUpdateResult::factory()->withWarnings()->count($count)->create()->reverse();
        $result2 = (new UpdateResultService())->getLatestWarnings($count);

        $this->assertEquals($count,$result2->count());
        $this->assertEquals($result1->sort()->pluck('warnings'),$result2->sort()->pluck('warnings'));
    }
}
