<?php

namespace Database\Factories\Avia;

use App\Models\Avia\AviaUpdateResult;
use Illuminate\Database\Eloquent\Factories\Factory;

class AviaUpdateResultFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AviaUpdateResult::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $url = $this->faker->unique()->url;
        $loaded = $this->faker->numberBetween(0, 100000);
        $warnings = json_encode($this->faker->sentences, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE);
        return [
            'md' => md5($url),
            'url' => $url,
            'loaded' => $loaded,
            'inserted' => $this->faker->numberBetween(0, $loaded),
            'error' => null,
            'warnings' => $this->faker->randomElement([null, $warnings]),
            'error_count' => $this->faker->numberBetween(0, 100),
        ];
    }

    /**
     * @return Factory
     */
    public function withError(): Factory
    {
        return $this->state([
            'error' => $this->getError(),
        ]);
    }

    /**
     * @return array
     */
    private function getError(): array
    {
        return [
            'message' => $this->faker->sentence,
            'code' => $this->faker->numberBetween(0, 1000),
            'file' => '/usr/home/www/' . $this->faker->word . '.php',
            'line' => $this->faker->numberBetween(0, 500),
            'trace' => $this->faker->text,
        ];
    }

    /**
     * @return Factory
     */
    public function withWarnings(): Factory
    {
        return $this->state([
            'warnings' => $this->getWarnings(),
        ]);
    }

    /**
     * @return array
     */
    private function getWarnings(): array
    {
        return [
            'data' => [
                'field1' => $this->faker->word,
                'field2' => $this->faker->word,
            ],
            'errors' => [
                'field1.1' => $this->faker->word,
                'field1.2' => $this->faker->word,
            ],
        ];
    }

}
