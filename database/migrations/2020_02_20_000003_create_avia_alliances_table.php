<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAviaAlliancesTable extends Migration
{
    private const TABLE_NAME = 'avia_alliances';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(static::TABLE_NAME, static function (Blueprint $table) {

            $table->string('name')->primary()
                ->comment('название альянса');
            $table->json('airlines')
                ->comment('коды компаний, входящих в альянс');
            $table->timestamp('created_at');
        });

        DB::statement('ALTER TABLE ' . static::TABLE_NAME .
            " COMMENT 'Альянсы'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(static::TABLE_NAME);
    }
}
