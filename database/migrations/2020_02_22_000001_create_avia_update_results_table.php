<?php

use App\Enums\Avia\DataType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAviaUpdateResultsTable extends Migration
{
    private const TABLE_NAME = 'avia_update_results';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(static::TABLE_NAME, static function (Blueprint $table) {
            $table->string('md', 32)->primary()
                ->comment('уникальный Хеш GET(POST) запроса');
            $table->enum('data_type', DataType::values())->index()
                ->comment('тип данных');
            $table->string('url', 2000)
                ->comment('url ресурса');
            $table->unsignedInteger('loaded')
                ->comment('получено строк')
                ->default(0);
            $table->unsignedInteger('inserted')
                ->comment('сохранено строк')
                ->default(0);
            $table->json('error')
                ->comment('ошибка')
                ->nullable();
            $table->json('warnings')
                ->comment('предупреждения')
                ->nullable();
            $table->unsignedInteger('error_count')
                ->comment('ошибочных попыток')
                ->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable()->index();
        });

        DB::statement('ALTER TABLE ' . static::TABLE_NAME .
            " COMMENT 'Результаты обновления данных'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(static::TABLE_NAME);
    }
}
