<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAviaRoutesTable extends Migration
{
    private const TABLE_NAME = 'avia_routes';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(static::TABLE_NAME, static function (Blueprint $table) {
            $table->increments('id');
            $table->string('airline_iata',2)->index()->nullable()
                ->comment(' IATA-код авиакомпании');
            $table->string('departure_airport_iata',3)->index()->nullable()
                ->comment('IATA-код аэропорта отправления');
            $table->string('arrival_airport_iata',3)->index()->nullable()
                ->comment('IATA-код аэропорта прибытия');
            $table->string('airline_icao',3)->index()->nullable()
                ->comment(' ICAO-код авиакомпании');
            $table->string('departure_airport_icao',3)->index()->nullable()
                ->comment('ICAO-код аэропорта отправления');
            $table->string('arrival_airport_icao',3)->index()->nullable()
                ->comment('ICAO-код аэропорта прибытия');
            $table->boolean('codeshare')->index()
                ->comment('показывает осуществляет ли рейс та же компания, что продает билет');
            $table->integer('transfers')->index()
                ->comment('количество пересадок');
            $table->json('planes')
                ->comment('IATA-коды самолетов');
            $table->timestamp('created_at');
        });

        DB::statement('ALTER TABLE ' . static::TABLE_NAME .
            " COMMENT 'Маршруты'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(static::TABLE_NAME);
    }
}
