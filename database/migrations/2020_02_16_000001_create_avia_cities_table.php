<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAviaCitiesTable extends Migration
{
    private const TABLE_NAME = 'avia_cities';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(static::TABLE_NAME, static function (Blueprint $table) {
            $table->string('code',3)->primary()
                ->comment('IATA код города');
            $table->string('country_code',2)->index()
                ->comment('IATA код страны, в которой находится город');
            $table->string('name_en')
                ->comment('название города EN');
            $table->string('name_ru')
                ->comment('название города RU');
            $table->string('time_zone')
                ->comment('часовой пояс относительно гринвича');
            $table->string('coordinates_lon')
                ->comment('координаты города - долгота');
            $table->string('coordinates_lat')
                ->comment('координаты города - широта');
            $table->json('cases')
                ->comment('падежи');
            $table->timestamp('created_at');
        });

        DB::statement('ALTER TABLE ' . static::TABLE_NAME .
            " COMMENT 'Города'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(static::TABLE_NAME);
    }
}
