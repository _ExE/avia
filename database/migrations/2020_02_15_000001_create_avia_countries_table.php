<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAviaCountriesTable extends Migration
{
    private const TABLE_NAME = 'avia_countries';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(static::TABLE_NAME, static function (Blueprint $table) {
            $table->string('code',2)->primary()
                ->comment('IATA код страны');
            $table->string('name_en')->comment('название страны EN');
            $table->string('name_ru')->comment('название страны RU');
            $table->string('currency',3)->comment('валюта страны');
            $table->json('cases')->comment('падежи');
            $table->timestamp('created_at');
        });

        DB::statement('ALTER TABLE ' . static::TABLE_NAME .
            " COMMENT 'Страны'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(static::TABLE_NAME);
    }
}
