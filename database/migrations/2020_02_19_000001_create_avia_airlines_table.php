<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAviaAirlinesTable extends Migration
{
    private const TABLE_NAME = 'avia_airlines';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(static::TABLE_NAME, static function (Blueprint $table) {
            $table->string('code',2)->primary()
                ->comment('IATA-код авиакомпании');
            $table->string('name_en')
                ->comment('название авиакомпании EN');
            $table->string('name_ru')
                ->comment('название авиакомпании RU');
            $table->timestamp('created_at');
        });

        DB::statement('ALTER TABLE ' . static::TABLE_NAME .
            " COMMENT 'Авиакомпании'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(static::TABLE_NAME);
    }
}
