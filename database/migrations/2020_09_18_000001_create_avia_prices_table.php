<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAviaPricesTable extends Migration
{
    private const TABLE_NAME = 'avia_prices';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(static::TABLE_NAME, static function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('origin_code', 3)->index()
                ->comment('пункт отправления');
            $table->string('destination_code', 3)->index()
                ->comment('пункт назначения');
            $table->boolean('show_to_affiliates')
                ->comment(
                    'False-все цены, true-только цены, найденные с помощью маркера партнера (рекомендуется)');
            $table->unsignedInteger('trip_class')->index()
                ->comment('Класс полета: 0-эконом-класс, 1 — Бизнес — класс, 2-первый класс');
            $table->date('depart_date')->index()
                ->comment('дата  отправления');
            $table->date('return_date')->index()->nullable()
                ->comment('дата  возвращения');
            $table->unsignedInteger('number_of_changes')->index()
                ->comment('количество пересадок');
            $table->string('value')->index()
                ->comment('Стоимость перелета, в указанной валюте');
            $table->timestamp('found_at')->comment('Время и дата, на которые был найден билет');
            $table->unsignedInteger('distance')
                ->comment('Расстояние между пунктом отправления и пунктом назначения');
            $table->boolean('actual')->index()
                ->comment('Актуальность предложения');
            $table->string('gate')
                ->comment('Похоже на источник информации');
            $table->timestamp('created_at')->index();
        });
        DB::statement('ALTER TABLE ' . static::TABLE_NAME .
            " COMMENT 'Цен, найденные пользователями за 48 часов'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(static::TABLE_NAME);
    }
}
