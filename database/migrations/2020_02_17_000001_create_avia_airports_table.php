<?php

use App\Enums\Avia\IataType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAviaAirportsTable extends Migration
{
    private const TABLE_NAME = 'avia_airports';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(static::TABLE_NAME, static function (Blueprint $table) {
            $table->string('code', 3)->primary()
                ->comment('IATA код');
            $table->string('country_code', 2)->index()
                ->comment('IATA код страны');
            $table->string('city_code', 3)->index()->nullable()
                ->comment('IATA код города');
            $table->enum('iata_type', IataType::values())->index()
                ->comment('IATA тип объекта');
            $table->boolean('flightable')->index()
                ->comment('является ли действующим аэропортом');
            $table->string('name_en')
                ->comment('название аэропорта EN');
            $table->string('name_ru')
                ->comment('название аэропорта RU');
            $table->string('time_zone')->nullable()
                ->comment('часовой пояс относительно гринвича');
            $table->string('coordinates_lon')->nullable()
                ->comment('координаты города - долгота');
            $table->string('coordinates_lat')->nullable()
                ->comment('координаты города - широта');
            $table->timestamp('created_at');
        });

        DB::statement('ALTER TABLE ' . static::TABLE_NAME .
            " COMMENT 'Аэропорты'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(static::TABLE_NAME);
    }
}
