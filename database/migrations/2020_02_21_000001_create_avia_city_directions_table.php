<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAviaCityDirectionsTable extends Migration
{
    private const TABLE_NAME = 'avia_city_directions';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(static::TABLE_NAME, static function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('origin_code', 3)->index()
                ->comment('пункт отправления');
            $table->string('destination_code', 3)->index()
                ->comment('пункт назначения');
            $table->unsignedInteger('price')->index()
                ->comment('стоимость перелета, в указанной валюте.');
            $table->unsignedInteger('transfers')->index()
                ->comment('количество пересадок');
            $table->string('airline_code', 2)->index()
                ->comment('IATA код авиакомпании');
            $table->string('flight_number')
                ->comment('номер рейса');
            $table->timestamp('departure_at')
                ->comment('дата и время отправления');
            $table->timestamp('return_at')
                ->comment('дата и время возвращения');
            $table->timestamp('expires_at')->index()
                ->comment('Дата истечения срока действия найденной цены (UTC+0)');
            $table->timestamp('created_at')->index();

            $table->unique(['origin_code','destination_code']);
        });
        DB::statement('ALTER TABLE ' . static::TABLE_NAME .
            " COMMENT 'Популярные направления из города'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(static::TABLE_NAME);
    }
}
